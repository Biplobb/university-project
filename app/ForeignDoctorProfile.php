<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForeignDoctorProfile extends Model
{
    protected $fillable=['name','speciality','degree','password','email','phone','address','hospital','member','awards','experience','photo'];
}
