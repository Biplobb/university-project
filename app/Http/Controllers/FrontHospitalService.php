<?php

namespace App\Http\Controllers;
use App\Hospital;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class FrontHospitalService extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Hospital=DB::table('hospital') ->orderBy('hospital.id', 'asc')->take(5)->get();
        return view('hospitalservice',compact('Hospital'));
    }
public function allindex()
    {
        $Hospital=DB::table('hospital')->get();
        return view('hospitalservice',compact('Hospital'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {





        $Hospital= DB::table('hospital')->
            select('*')->where('id', $id)
            ->get();

          $doctorprofile=DB::table('doctor_hospitals')->select('doctor_profiles.name as name','doctor_profiles.photo as dphoto','doctor_profiles.id as id','hospital.photo','doctor_profiles.address as doctor_address','experience')
            ->leftJoin('doctor_profiles','doctor_profiles.id','doctor_hospitals.doctor_id')
            ->leftJoin('hospital','hospital.id','doctor_hospitals.hospital_id')
            ->where('doctor_hospitals.hospital_id','=',$id)->get();






        return view('Hospitalshow',compact('Hospital','doctorprofile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allhospitaldoctor($id)
    {


        $Hospital= DB::table('hospital')
        ->where('id', $id)
        ->first();

        $doctorprofile=DB::table('doctor_profiles')->select('name','hospital','id','speciality','phone as doctor_phone','doctor_profiles.address as doctor_address','speciality','member','awards','experience','photo')
            ->where('doctor_profiles.hospital',$id)->get();
        return view('hospitalshow',compact('Hospital','doctorprofile'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function searchhospital(Request $request)
    {

        $option ='';
        $data=DB::table('hospital')->select('name','id','location')
            ->where('name','like','%'.$request->keyword.'%')->get();

        $option .="<ul class='datalist'>";
        if(sizeof($data)>0){
            foreach ($data as $d)
            {
                $option .="<li class='lists'  data-id='$d->id' data-name='$d->name'>".$d->name."<p>$d->location</p></li>";
            }
        }else{
            $option .="<li>No data found</li>";
        }
        $option .="</ul>";
        return $option;

//        return $request;
    }
}
