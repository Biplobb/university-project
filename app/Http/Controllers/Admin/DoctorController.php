<?php

namespace App\Http\Controllers\Admin;

use App\DoctorProfile;
use App\Hospital;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class DoctorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }

    public function index()
    {
        $doctorprofile=DB::table('doctor_profiles')->select('name','id','speciality','phone as doctor_phone','doctor_profiles.address as doctor_address','speciality','member','awards','experience','photo')
            ->get();
        return view('admin/doctor/index',compact('doctorprofile'));
    }

    public function create()
    {
        $data=DB::table('hospital')->select('id','name')->get();
        return view('admin.doctor.create',compact('data'));
    }

    public function show($id)
    {
        $doctorprofile=DB::table('doctor_profiles')
            ->where('doctor_profiles.id','=',$id)
            ->select('name','id','email','speciality','phone','address','speciality','member','awards','experience','photo','degree')
            ->get()->first();

        $timeschedule=DB::table('dcotor_schedules')
            ->leftJoin('doctor_profiles','doctor_profiles.id','dcotor_schedules.doctor_id')
            ->leftJoin('hospital','hospital.id','dcotor_schedules.hospital_id')
            ->where('doctor_profiles.id','=',$id)
            ->select('start','end','hospital.name as hospitalname','interval')
            ->get();

        $chamber=DB::table('doctor_hospitals')
            ->leftJoin('doctor_profiles','doctor_profiles.id','doctor_hospitals.doctor_id')
            ->leftJoin('hospital','hospital.id','doctor_hospitals.hospital_id')
            ->where('doctor_profiles.id','=',$id)
            ->select('hospital.name as chamber','location')
            ->get();
            return view('/admin/doctor/show',compact('doctorprofile','chamber','timeschedule'));
    }
    public function store(Request $request)
    {
         
        $id='';

        $request->validate([
            'email' => 'required|unique:doctor_profiles|max:35',
            'password' => 'required|min:6',
            'speciality' => 'required',
            'phone' => 'required|unique:doctor_profiles',
            'address' => 'required',
            'experience' => 'required',
        ]);


        if($request->hasFile('photo'))
        {
            $destinationPath="image/doctor-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            Image::make($file)->resize(200,200)->save($file->move($destinationPath,$filename));
        }

        $data=['name'=>$request->name,
            'speciality'=>$request->speciality,
            'degree'=>$request->degree,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'phone'=>$request->phone,
            'address'=>$request->address,
            'member'=>$request->member,
            'awards'=>$request->awards,
            'experience'=>$request->experience,
            'photo'=>$filename];



        if(DoctorProfile::create($data))
        {
            $id=DoctorProfile::select('id')->orderBy('created_at','desc')->get()->first();
            for($i=0;$i<sizeof($request->hospital);$i++)
            {
                DB::table('doctor_hospitals')->insert(['doctor_id'=>$id->id,'hospital_id'=>$request->hospital[$i],'assigned'=>'no','first_fees'=>null,'second_fees'=>null,'created_at'=>Carbon::now(),'updated_at'=>Carbon::now()]);
            }
        }
        return redirect(route('doctorschedule',['id' => $id->id]));

    }
    public function edit($id)
    {
        $doctorprofile=DoctorProfile::find($id);
        return view('/admin/doctor/edit',compact('doctorprofile'));
    }

    public function update(Request $request, $id)
    {
        $oldimage=DoctorProfile::select('photo')->find($id);


        if($request->hasFile('photo'))
        {
            $file=$request->photo;
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            Image::make($file)->resize(200,200)->save($file->move('image/doctor-photo/',$filename));
            $photo=$filename;
            $filename=($photo);
            Storage::delete('doctor-photo/'.$oldimage->photo);
            $data=['name'=>$request->name,
                'speciality'=>$request->speciality,
                'degree'=>$request->degree,
                'phone'=>$request->phone,
                'address'=>$request->address,
                'member'=>$request->member,
                'awards'=>$request->awards,
                'experience'=>$request->experience,
                'photo'=>$photo];
            DB::table('doctor_profiles')
                ->where('id', $id)
                ->update($data);
            return redirect('doctorindex');
        }else{
             $data=['name'=>$request->name,
                'speciality'=>$request->speciality,
                'degree'=>$request->degree,
                'phone'=>$request->phone,
                'address'=>$request->address,
                'member'=>$request->member,
                'awards'=>$request->awards,
                'experience'=>$request->experience,
                'photo'=>$oldimage->photo];
            DB::table('doctor_profiles')
                ->where('id', $id)
                ->update($data);
            return redirect(route('doctorindex'))->with('updatesuccess','Info updated successfully !!');
        }
    }
    public function destroy($id)
    {
        $oldimage=DoctorProfile::select('photo')->find($id);
        Storage::delete('doctor-photo/'.$oldimage->photo);
        doctorprofile::destroy($id);
        return redirect(route('doctorindex'))->with('deletesuccess','Info deleted successfully !!');
    }

    public function addorremovechamber()
    {
        $hospitals=DB::table('hospital')->select('name','id')->get();
        $doctors=DoctorProfile::select('name','id')->get();
        return view('admin.doctor.addorremovechamber',compact('hospitals','doctors'));
    }

    public function postaddorremovechmaber(Request $request)
    {

        switch ($request->opt_type)
        {
            case 'add':

                $data=DB::table('doctor_hospitals')->select('id')->where('doctor_id', '=', $request->doctor_id)->where('hospital_id','=',$request->hospital_id)->get();
                if(sizeof($data)==1){
                    return redirect()->back()->with('exist','Data already exist !!');
                }else{
                    DB::table('doctor_hospitals')->insert(['doctor_id'=>$request->doctor_id,'hospital_id'=>$request->hospital_id,'assigned'=>'no','created_at'=>Carbon::now(),'updated_at'=>Carbon::now()]);
                    return redirect(route('doctorschedule',['id' => $request->doctor_id]))->with('hospitaladded','Hospital added !! Add time schedule !!');
                }

                break;
            case 'remove':
                DB::table('doctor_hospitals')->where('doctor_id', '=', $request->doctor_id)->where('hospital_id','=',$request->hospital_id)->delete();
                return redirect()->back()->with('delete','Data removed !!');
                break;
        }
    }



}
