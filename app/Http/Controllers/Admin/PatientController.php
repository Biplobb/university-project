<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function index()
    {
        return view('admin.patient.index');
    }

    public function create()
    {
        return view('admin.patient.create');
    }

    public function show()
    {
        return view('admin.patient.show');
    }
}
