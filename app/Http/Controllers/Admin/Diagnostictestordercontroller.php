<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use PDF;
use App\tests;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;

class Diagnostictestordercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }
    public function index()
    {
       /* $user_info = DB::table('usermetas')
            ->select('browser', DB::raw('count(*) as total'))
            ->groupBy('browser')
            ->get();*/
      $diagnostic_center= DB::table('testorder')->select('testorder.name as testordername','testorder.id as testorderid','testorder.address as testorderaddress','testorder.phone as testorderphone','tests.name as testsname','tests.price as testsprice')-> leftJoin('tests','testorder.Tests_id','tests.id')
           ->get();
       return view('admin.Testorder.index',compact('diagnostic_center'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getstore(Request $request)
    {



        for ($i = 0; $i < sizeof($request->tests); $i++) {
            $order=DB::table('tests')->select('tests.name')->where(['tests.id'=>$request->tests[$i]]);
        }
return view('orderview',compact('order[]'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       /* return DB::table('tests')->select('tests.name')->where('id','=','1')->get();*/

        $ids=[];
         $tests=[];
        if ($request->hasFile('photo')) {
            $destinationPath = "image/testorder-photo";
            $file = $request->file('photo');
            $extention = $file->getClientOriginalExtension();
            $filename = rand(111111, 999999) . "." . $extention;
            $success = Image::make($file)->resize(200, 200)->save($file->move($destinationPath, $filename));
        }

        if (!empty($request->file('photo')) & !empty($request->tests)) {
            for ($i = 0; $i < sizeof($request->tests); $i++) {

                $ids[] = DB::table('testorder')->insertGetId(['Tests_id' => $request->tests[$i], 'photo' => $filename, 'name' => $request->name, 'status' => '0', 'Diagnostic_Center_id' => $request->Diagnostic_Center_id, 'address' => $request->address, 'phone' => $request->phone, 'created_at' => now(), 'updated_at' => now()]);

            }
        }
        elseif(!empty($request->tests)){
            for ($i = 0; $i <1; $i++) {

                $ids[] = DB::table('testorder')->insertGetId(['Tests_id' => $request->tests[$i],'name' => $request->name, 'status' => '0', 'Diagnostic_Center_id' => $request->Diagnostic_Center_id, 'address' => $request->address, 'phone' => $request->phone, 'created_at' => now(), 'updated_at' => now()]);

            }


        }

          elseif(!empty($request->file('photo'))) {
                 for ($i = 0; $i <1; $i++) {

                     $ids[] = DB::table('testorder')->insertGetId(['photo' => $filename, 'name' => $request->name, 'status' => '0', 'Diagnostic_Center_id' => $request->Diagnostic_Center_id, 'address' => $request->address, 'phone' => $request->phone, 'created_at' => now(), 'updated_at' => now()]);

                 }

             }
        else
        {
echo "please again try";
        }





           $diagnostic_center=DB::table('diagnostic_center')->select('*')->where(['diagnostic_center.id'=>$request->Diagnostic_Center_id])->get();

            for ($i = 0; $i<sizeof($ids); $i++) {
                $tests[]= DB::table('testorder')->select('*')->where(['id'=>$ids[$i]])->get();
            }
            for ($i = 0; $i<sizeof($ids); $i++) {

                $tests[]= DB::table('tests')->select('*')->where(['id'=>$ids[$i]])->get();
            }
            for ($i = 0; $i<sizeof($ids); $i++) {
                $testss[]= DB::table('testorder')->select('*')->where(['id'=>$ids[$i]])->get();
            }
            for ($i = 0; $i<sizeof($ids); $i++) {

                $testss[]= DB::table('tests')->select('*')->where(['id'=>$ids[$i]])->get();
            }

      /*  $diagnostic_center_tests[]=DB::table('diagnostic_center_tests')->select('tests.name as testname','diagnostic_center_tests.price as diagnosticcentertestsprice','diagnostic_center_tests.discountprice as diagnosticcentertestsdiscountprice','tests.id as testid')->

        leftJoin('tests','diagnostic_center_tests.tests_id','tests.id')
            ->where(['diagnostic_center_tests.tests_id'=>$ids[$i]])->get();*/

            for ($i = 0; $i<sizeof($ids); $i++) {


               $testss[]= DB::table('tests')->select('*')->where(['id'=>$ids[$i]])->get();
            }for ($i = 0; $i<sizeof($ids); $i++) {


               $tests[]= DB::table('tests')->select('*')->where(['id'=>$ids[$i]])->get();
            }
//           $testorder= DB::table('testorder')->select('*')->where(['testorder.phone'=>$request->phone])->get();


//            return view('testpdf',compact('diagnostic_center','tests'));
       // $diagnostics=DB::table('diagnostic_center')->select('*')->get();
        return view('testpdf',compact('diagnostic_center','tests','testss'));



        }




    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('testorder')
            ->where('id', $id)
            ->delete();


        return redirect('admin/testorder/');
    }
}
