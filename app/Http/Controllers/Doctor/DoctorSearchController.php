<?php

namespace App\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DoctorSearchController extends Controller
{
    public function search($sp)
    {
        // $data=DoctorProfile::select('id','name','speciality','phone','address','photo')->where('speciality','=',$sp)->get();
        $data=DB::table('doctor_hospitals')
        
            ->leftJoin('doctor_profiles','doctor_profiles.id','=','doctor_hospitals.doctor_id')
            ->leftJoin('hospital','hospital.id','=','doctor_hospitals.hospital_id')
            ->where('speciality','=',$sp)
            ->select('doctor_profiles.id as did','doctor_profiles.name as dname','degree',
            'speciality','doctor_profiles.phone as dphone','address','doctor_profiles.photo as dphoto','hospital.name as hname')
            ->distinct('did')
            ->paginate(10);
            
        if(sizeof($data)==0){
            return view('404');
        }
        return view('search',compact('data'));
    }

    public function alldoctors()
    {
        $data=DB::table('doctor_hospitals')
            ->leftJoin('doctor_profiles','doctor_profiles.id','=','doctor_hospitals.doctor_id')
            ->leftJoin('hospital','hospital.id','=','doctor_hospitals.hospital_id')
            ->select('doctor_profiles.id as did','doctor_profiles.name as dname','degree',
                'speciality','doctor_profiles.phone as dphone','address','doctor_profiles.photo as dphoto','hospital.name as hname')
            ->distinct('did')
            ->paginate(10);
        if(sizeof($data)==0){
            return view('404');
        }
        return view('search',compact('data'));
    }
}
