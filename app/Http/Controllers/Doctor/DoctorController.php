<?php

namespace App\Http\Controllers\Doctor;

use App\DoctorProfile;
use App\DoctorRequestToJoin;
use Dompdf\FrameDecorator\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DoctorController extends Controller
{

    public  function __construct()
    {
        $this->middleware('auth:doctor')->except('join');
    }

    public function index()
    {
        $doctorprofile=DB::table('doctor_profiles')
            ->where('doctor_profiles.id','=',Auth::guard('doctor')->user()->id)
            ->select('name','id','email','speciality','phone','address','speciality','member','awards','experience','photo')
            ->get()
            ->first();
        return view('doctor/dashboard',compact('doctorprofile'));
    }

    public function doctorchamber()
    {
        $data=DB::table('dcotor_schedules')
            ->leftJoin('doctor_profiles','doctor_profiles.id','dcotor_schedules.doctor_id')
            ->leftJoin('hospital','hospital.id','dcotor_schedules.hospital_id')
            ->leftJoin('days','days.id','dcotor_schedules.day_id')
            ->where('doctor_profiles.id','=',Auth::guard('doctor')->user()->id)
            ->select('dcotor_schedules.id as scheduleid','hospital.name as chamber','hospital.id as hospitalid','location','start','end','day_name','days.id as day_id','available')
            ->get();
        return view('doctor.doctorchamber',compact('data'));
    }



    public function updateschedulebydoctor(Request $request)
    {
        DB::table('dcotor_schedules')->where('id','=',$request->scheduleid)
            ->update(['start'=>$request->start,'end'=>$request->end,'day_id'=>$request->day,'available'=>$request->available]);
        return redirect()->back()->with('updated','Schedule updated');
    }

    public function update(Request $request)
    {
        DB::table('doctor_profiles')->where('id','=',Auth::guard('doctor')->user()->id)
            ->update(['name'=>$request->name,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'address'=>$request->address,
                'member'=>$request->member,
                'awards'=>$request->awards,
                'experience'=>$request->experience,
            ]);
        return redirect()->back()->with('updated','Profile updated');
    }

    public function join(Request $request)
    {
        $drj=new  DoctorRequestToJoin;

        if($request->termsaccept=='yes') {

            $validatedData = $request->validate([
                'name' => 'required',
                'email' => 'required|unique:doctor_request_to_joins',
                'designation' => 'required',
                'specialty' => 'required',
                'gender' => 'required',
                'phone' => 'required|unique:doctor_request_to_joins',
                'location' => 'required',
                'bmdc_reg_no' => 'required|unique:doctor_request_to_joins',
                'nid' => 'required|unique:doctor_request_to_joins',
                'interestedat' => 'required'
            ]);

            if ($validatedData) {
                if ($request->hasFile('image')) {
                    $destinationPath = "image/newdoctor-photo";
                    $file = $request->file('image');
                    $extention = $file->getClientOriginalExtension();
                    $filename = $request->input('bmdc_reg_no') . "." . $extention;
                    \Intervention\Image\Facades\Image::make($file)->resize(200, 200)->save($file->move($destinationPath, $filename));
                }

                $drj->name = $request->input('name');
                $drj->email = $request->input('email');
                $drj->designation = $request->input('designation');
                $drj->specialty = $request->input('specialty');
                $drj->gender = $request->input('gender');
                $drj->phone = $request->input('phone');
                $drj->location = $request->input('location');
                $drj->bmdc_reg_no = $request->input('bmdc_reg_no');
                $drj->nid = $request->input('nid');
                $drj->degree = $request->input('degree');
                $drj->experience = $request->input('experience');
                $drj->interestedat = implode(',', $request->input('interestedat'));
                $drj->image = $filename;
                $drj->save();
                return redirect()->back()->with('success', 'Thank you !! Your request is recorded, We will contact with you soon.');
            } else {
                return redirect()->back()->withErrors($validatedData);
            }
        }
        return redirect()->back();

    }

}
