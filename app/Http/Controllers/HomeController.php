<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        return view('index');
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addnewdoctor()
    {
        return view('doctorregister');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function interview()
    {
        return view('interview');
    }
    public function shop()
    {
        return view('shop');
    }


    public function equipment()
    {
        return view('equipment');
    }
    public function india()
    {
        return view('india');
    }


    public function about(){
        return view('about');
    }

    public function appointment(){
        return view('appointment');
    }

    public function faq(){
        return view('faq');
    }

    public function services(){
        return view('services');
    }
    public function doctors(){
        return view('doctors');
    }
    public function features(){
        return view('features');
    }
    public function blog(){
        return view('blog');
    }
    public function awards(){
        return view('awards');
    }

    public function contacts(){
        return view('contact');
    }

    public function others(){
        return view('others');
    }

    public function generalhospital(){
        return view('generalhospital');
    }
    public function dentalhospital(){
        return view('dental');
    }

}
