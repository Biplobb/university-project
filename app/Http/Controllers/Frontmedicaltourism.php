<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class Frontmedicaltourism extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  $Hospital=DB::table('medicaltourism') ->orderBy('medicaltourism.id', 'asc')->take(5)->get();
        return view('medicaltourism',compact('Hospital'));
    }
    public function allindex()
    {  $Hospital=DB::table('medicaltourism')->get();
        return view('medicaltourism',compact('Hospital'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Hospital= DB::table('medicaltourism')->
        select('*')->where('id', $id)
            ->get();

        $doctorprofile=DB::table('foreigndoctor_medicaltourism')->select('foreigndoctor.name as name','foreigndoctor.photo as dphoto','foreigndoctor.id as id','foreigndoctor.overview as overview')
            ->leftJoin('foreigndoctor','foreigndoctor.id','foreigndoctor_medicaltourism.foreigndoctor_id')
            ->leftJoin('medicaltourism','medicaltourism.id','foreigndoctor_medicaltourism.medicaltourism_id')
            ->where('foreigndoctor_medicaltourism.medicaltourism_id','=',$id)->get();






        return view('singlemedicaltourism',compact('Hospital','doctorprofile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
