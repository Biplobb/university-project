@extends('layouts.master')

@section('content')
    <section id="breadcrumb" class="space light-overlay" data-stellar-background-ratio="0.4">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bread-block animate-in move-up">
                    <h2>Awards</h2>
                    <p>Affordable Treatments, Honest & Experienced Dentists.</p>
                </div>
                <div class="col-sm-6 bread-block text-right animate-in move-up">
                    <a href="#" class="btn">Book appointment</a>
                </div>
            </div>
        </div>
    </section>
    <!--Awards-->
    <section id="awards" class="space">
        <div class="container">
            <div class="row">
                <div class="award-base col-sm-8 col-sm-offset-2 no-padding">
                    <div class="award-block col-sm-12 no-padding">
                        <div class="award-item col-sm-6 text-right animate-in left-out">
                            <div class="date">1992</div>
                        </div>
                        <div class="award-item col-sm-6 animate-in right-out">
                            <h4>Morbi laoreet lectus quam. </h4>
                            <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus </p>
                            <img src="{{asset('front-end/assets/images/award.jpg')}}" alt="Columba">
                        </div>
                    </div>
                    <div class="award-block col-sm-12 no-padding">
                        <div class="award-item col-sm-6 animate-in left-out">
                            <h4>Morbi laoreet lectus quam. </h4>
                            <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus </p>
                            <img src="{{asset('front-end/assets/images/award.jpg')}}" alt="Columba">
                        </div>
                        <div class="award-item col-sm-6 animate-in right-out">
                            <div class="date">1994</div>
                        </div>
                    </div>
                    <div class="award-block col-sm-12 no-padding">
                        <div class="award-item col-sm-6 text-right animate-in left-out">
                            <div class="date">1992</div>
                        </div>
                        <div class="award-item col-sm-6 animate-in right-out">
                            <h4>Morbi laoreet lectus quam. </h4>
                            <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus </p>
                            <img src="{{asset('front-end/assets/images/award.jpg')}}" alt="Columba">
                        </div>
                    </div>
                    <div class="award-block col-sm-12 no-padding">
                        <div class="award-item col-sm-6 animate-in left-out">
                            <h4>Morbi laoreet lectus quam. </h4>
                            <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus </p>
                            <img src="{{asset('front-end/assets/images/award.jpg')}}" alt="Columba">
                        </div>
                        <div class="award-item col-sm-6 animate-in right-out">
                            <div class="date">1995</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection