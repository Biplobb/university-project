@extends('admin.layouts.master')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>All Donar</h2>
            <small class="text-muted">Welcome to Swift application</small>
            <div class="block-header">
            </div>
        </div>


<table class="table table-bordered table-hover">
    <thead>
    <tr style="background:#1BC3E1">
        <th scope="col">SL</th>
        <th scope="col">Donor Name</th>
        <th scope="col">Donor address</th>
        <th scope="col">Donor phone Number</th>
        <th scope="col">Last Donate date</th>
        <th scope="col"> Blood_group</th>
        <th scope="col">Donor Email</th>
        <th scope="col">Donor District</th>
        <th scope="col">Donor Upozila</th>
        <th scope="col">Donor Status</th>
        <th scope="col">Action</th>

    </tr>
    </thead>
    <tbody>
    @foreach($donars as $donars)

<tr>
    <th scope="row"> {{ $donars->id }}</th>
    <td>{{ $donars->name }}</td>
    <td>{{ $donars->address }}</td>
    <td>{{ $donars->phone }}</td>
    <td>{{ $donars->date }}</td>
    <td>{{ $donars->blood_group }}</td>
    <td>{{ $donars->email }}</td>
    <td>{{ $donars->area }}</td>
    <td>{{ $donars->upozila }}</td>
    <td>{{$donars->status}}</td>
    <td>
        <form action="{{url('admin/blood/pendingdonor/confirm/'.$donars->id)}}" method="post" enctype="multipart/form-data">

            {{csrf_field()}}

            <button type="submit" class="btn btn-raised g-bg-cyan">Confirm</button>

        </form>

        <form action="{{url('admin/blood/pendingdonor/delete/'.$donars->id)}}" method="post" enctype="multipart/form-data">

            {{csrf_field()}}

            <button type="submit" class="btn btn-raised g-bg-cyan" style="background:#954120" onclick="return confirm('Are you sure to Delete This');">Delete</button>

        </form>



            </td>
        </tr>
    @endforeach

    </tbody>
</table>
</div>
   </div>
        </div>
    </div>
</div>
</div>
</div>
</section>
@endsection


