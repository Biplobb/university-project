{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>All Donar</h2>
            <small class="text-muted">Welcome to Swift application</small>
            <div class="block-header">
            </div>
        </div>
<div class="row clearfix">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <div class="card">
    <div class="body">
      <div class="member-card verified">
<div class="table-responsive">
<table class="table table-bordered table-hover">
<thead>
<tr style="background:#1BC3E1">
    <th scope="col">SL</th>
    <th scope="col">Donor Name</th>
    <th scope="col">Donor address</th>
    <th scope="col">Donor phone Number</th>
    <th scope="col">Last Donate date</th>
    <th scope="col"> Blood_group</th>
    <th scope="col">Donor Email</th>
    <th scope="col">Donor District</th>
    <th scope="col">Donor Upozila</th>
    <th scope="col">Donor Status</th>
    <th scope="col">Action</th>
  </tr>
    </thead>
      <tbody>

@if(sizeof($donars)==0)
    <h2>No data found</h2>
    @else
@foreach($donars as $donar)

    <tr>
        <th scope="row"> {{ $donar->id }}</th>
        <td>{{ $donar->name }}</td>
        <td>{{ $donar->address }}</td>
        <td>{{ $donar->phone }}</td>
        <td>{{ $donar->date }}</td>
        <td>{{ $donar->blood_group }}</td>

        <td>{{ $donar->email }}</td>
        <td>{{ $donar->area }}</td>
        <td>{{ $donar->upozila }}</td>
        <td>{{$donar->status==1 ?'Published':'Unpublished'}}</td>
        <td style="width:250px">
            <a href="{{url('/admin/blood/edit/'.$donar->id)}}"  class="btn  btn-raised bg-orange btn-block btn-xs waves-effect">Edit</a>
            <a href="{{url('/admin/blood/show/'.$donar->id)}}"  class="btn  btn-raised bg-teal btn-block btn-xs waves-effect">Show</a>
            <a href="{{url('/admin/blood/delete/'.$donar->id)}}"  class="btn  btn-raised bg-red btn-block btn-xs waves-effect" onclick="return confirm('Are you sure to Delete This');">Delete</a>
        </td>
    </tr>
  @endforeach
   @endif
   </tbody>
     </table>
<ul class="pagination">
    <div class="col-sm-12 co-pagination animate-in move-up animated">
        {{$donars->links()}}
    </div>
</ul>
  </div>
    </div>
     </div>
      </div>
        </div>
        </div>
    </div>
    </section>
    @endsection


{{--@include('admin.layouts.footer')--}}