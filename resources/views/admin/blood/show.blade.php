@extends('admin.layouts.master')

@section('content')
    <ul>
            <li>{{$donar->name}}</li>

        <li>Action</li>
    </ul>
    <section class="content profile-page">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 p-l-0 p-r-0">
                    <section class="boxs-simple">
                        <div class="profile-header">
                            <div class="profile_info">

                                <h4 class="mb-0"><strong> </strong></h4>
                                <span class="text-muted col-white">{{$donar->name}}</span>
                                <div class="mt-10">
                                    <button class="btn btn-raised btn-default bg-blush btn-sm">Follow</button>
                                    <button class="btn btn-raised btn-default bg-green btn-sm">Message</button>
                                </div>
                                <p class="social-icon">
                                    <a title="Twitter" href="#"><i class="zmdi zmdi-twitter"></i></a>
                                    <a title="Facebook" href="#"><i class="zmdi zmdi-facebook"></i></a>
                                    <a title="Google-plus" href="#"><i class="zmdi zmdi-twitter"></i></a>
                                    <a title="Dribbble" href="#"><i class="zmdi zmdi-dribbble"></i></a>
                                    <a title="Behance" href="#"><i class="zmdi zmdi-behance"></i></a>
                                    <a title="Instagram" href="#"><i class="zmdi zmdi-instagram "></i></a>
                                    <a title="Pinterest" href="#"><i class="zmdi zmdi-pinterest "></i></a>
                                </p>
                            </div>
                        </div>
                        <div class="profile-sub-header">
                            <div class="box-list">
                                <ul class="text-center">
                                    <li> <a href="mail-inbox.html" class=""><i class="zmdi zmdi-email"></i>
                                            <p>My Inbox</p>
                                        </a> </li>
                                    <li> <a href="#" class=""><i class="zmdi zmdi-camera"></i>
                                            <p>Gallery</p>
                                        </a> </li>
                                    <li> <a href="#"><i class="zmdi zmdi-attachment"></i>
                                            <p>Collections</p>
                                        </a> </li>
                                    <li> <a href="#"><i class="zmdi zmdi-format-subject"></i>
                                            <p>Tasks</p>
                                        </a> </li>
                                </ul>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="body">
                            <h4>About Me</h4>
                            {{--<table class="table">--}}
                                <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">SL</th>
                                    <th scope="col">Donor Name</th>
                                    <th scope="col">Donor address</th>
                                    <th scope="col">Donor phone Number</th>
                                    <th scope="col">Last Donate date</th>
                                    <th scope="col">Donor Email</th>
                                    <th scope="col">Donor District</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <th scope="row"> {{ $donar->id }}</th>
                                    <td>{{ $donar->name }}</td>
                                    <td>{{ $donar->address }}</td>
                                    <td>{{ $donar->phone }}</td>
                                    <td>{{ $donar->date}}</td>
                                    <td>{{ $donar->email }}</td>
                                    <td>{{ $donar->area}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection


