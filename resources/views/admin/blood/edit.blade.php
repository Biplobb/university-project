@extends('admin.layouts.master')
@section('content')

<section class="content">
<div class="container-fluid">
<div class="block-header">
<p class="text-align:center text-success">{{Session::get('message')}}</p>
</div>

<div class="row clearfix">
{!!   Form::open(['url'=>'/admin/blood/update/'.$donar->id,'method'=>'patch','class'=>'form-horizontal'])!!}
<div class="col-md-12">
    <div class="card">
        <div class="header">
            <h2>Basic Information <small>Description text here...</small> </h2>
        </div>
        <div class="body">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" name="name" class="form-control" placeholder="Full Name"  value="{{$donar->name}}">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" name="address" class="form-control" placeholder="Address" value="{{$donar->address}}">
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="form-line">
                        <select class="form-control" name="area">

                            <option value="" checked>{{ $donar->area }}</option>
                            @foreach ($district as $district)
                                {
                                <option value="{{ $district->name}}">{{ $district->name }}</option>
                                }
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="form-line">
                        <select class="form-control" name="upozila">

                            <option value="" checked>{{ $donar->upozila }}</option>
                            @foreach ($upozila as $upozila)
                                {
                                <option value="{{ $upozila->name}}">{{ $upozila->name }}</option>
                                }
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{$donar->phone}}">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" name="date" class="form-control" placeholder="date"value="{{$donar->date}}">
                    </div>
                </div>
            </div>

    <div class="col-sm-6 col-xs-12">
        <div class="form-group">
            <div class="form-line">
                <label for="">Blood Group</label>
            <input type="radio" id="contactChoice2"
                   name="blood_group" value="O+" {{ $donar->blood_group=='O+'? "checked='checked'":''}}>
            <label for="contactChoice2">O+</label>
            <input type="radio" id="contactChoice3"
                   name="blood_group" value="O-" {{ $donar->blood_group=='O-'? "checked='checked'":'' }}>
            <label for="contactChoice3">O-</label>
            <input type="radio" id="contactChoice4"
                   name="blood_group" value="A+" {{$donar->blood_group=='A+'? "checked='checked'":'' }}>
            <label for="contactChoice4">A+</label>
            <input type="radio" id="contactChoice5"
                   name="blood_group" value="A-" {{  $donar->blood_group=='A-'? "checked='checked'":'' }}>
            <label for="contactChoice5">A-</label>
            <input type="radio" id="contactChoice8"
                   name="blood_group" value="B+" {{ $donar->blood_group=='B+'? "checked='checked'":'' }}>
            <label for="contactChoice8">B+</label>
            <input type="radio" id="contactChoice9"
                   name="blood_group" value="B-"  {{$donar->blood_group=='B-'? "checked='checked'":'' }}>
            <label for="contactChoice9">B-</label>
            <input type="radio" id="contactChoice6"
                   name="blood_group" value="AB+" {{$donar->blood_group=='AB+'? "checked='checked'":''}}>
            <label for="contactChoice6">AB+</label>
            <input type="radio" id="contactChoice7"
                   name="blood_group" value="AB-" {{ $donar->blood_group=='AB-'? "checked='checked'":''}}>
            <label for="contactChoice7">AB-</label>
        </div>
    </div>
</div>

        <div class="col-sm-6 col-xs-12">
            <div class="form-group">
                <div class="form-line"> <input type="email" name="email" class="form-control" placeholder="Your Email" value="{{$donar->email}}">

                </div>
            </div>
        </div>

    <div class="col-xs-12">
        <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
        <button type="submit" class="btn btn-raised">Cancel</button>
    </div>
    {!!  Form::close()!!}
</div>
</div>
</div>
</div>
</div>
</section>
@endsection