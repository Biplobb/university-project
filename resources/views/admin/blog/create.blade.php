@extends('admin.layouts.master')
@section('content')

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>Add Blog</h2>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">


    <form action="{{route('blogPost')}}" method="post" enctype="multipart/form-data">

{{csrf_field()}}

                        <div class="body">
                            <div class="row clearfix">



                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="">Doctor id</label>
                                            <select class="form-control" name="doctor_id"   data-parsley-required="true" placeholder="  Name">
                                                @foreach ($doctorprofile as $doctorprofile)
                                                    {

                                                    <option value="{{ $doctorprofile->id }}">{{ $doctorprofile->name }}</option>
                                                    }
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder=" Title"  name="title">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        {{Form::select('category_id',$categories,null,['class' => 'form-control','placeholder' => 'Select Category'])}}
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Author" name="author">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-control">

                                        <select name="type" id="" class="form-control">
                                            <option>Type</option>
                                            <option value="1">Featured</option>
                                            <option value="0">Regular</option>

                                        </select>



                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-control">

                                        <label for="Image">Insert image</label>
                                        <input type="file" name="image" >
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea rows="4" class="form-control no-resize" placeholder="Type your blog post here....." name="blog_post"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                            <button type="submit" class="btn btn-raised">Cancel</button>
                        </div>

    </form>

                    </div>

                </div>
            </div>
        </div>
        </div>


    </section>
@endsection

