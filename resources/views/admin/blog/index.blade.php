@extends('admin.layouts.master')
@section('content')

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>Blog List</h2>
            </div>


            @if(session()->has('message'))
                {{ session('message') }}
            @endif
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>


                            <th> id</th>
                            <th>Doctor id</th>
                            <th>Blog title</th>
                            <th>Category/th>
                            <th>Author</th>
                            <th>Blog post</th>
                            <th>Type</th>
                            <th>Blog Image</th>
                            <th>Action</th>


                        </tr>
                        </thead>
                        <tbody>

                        @foreach($blog as $b)
                            <tr>
                                <td>{{ $b->id }}</td>
                                <td>{{ $b->doctor_id }}</td>
                                <td>{{ $b->title }} </td>
                                <td>{{ $b->category }}</td>
                                <td>{{ $b->author }}</td>
                                <td>{!! $b->blog_post !!} </td>
                                <td>{{ $b->type }}</td>
                                <td><img src="{{asset('image/blog-photo/'.$b->image)}}" width="400px",height="400px"></td>

                                <td>
                                    <a href="{{route('showBlog',['id'=>$b->id])}}">Show</a>
                                    <a href="{{route('editBlog',['id'=>$b->id])}}">Edit</a>
                                    <a href="{{route('destroyBlog',['id'=>$b->id])}}">Delete</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        </div>
        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}