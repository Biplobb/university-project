@extends('admin.layouts.master')
@section('content')

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>Blog Comment List</h2>
            </div>



            <div class="body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>


                            <th>blog_id</th>
                            <th>comments</th>
                            <th>name</th>
                            <th>email</th>
                            <th>status</th>



                        </tr>
                        </thead>
                        <tbody>

                        @foreach($comment as $c)
                            <tr>
                                <td>{{ $c->id }}</td>
                                <td>{{ $c->blog_id }}</td>
                                <td>{{ $c->name }} </td>
                                <td>{{ $c->email }}</td>
                                <td>{{ $c->status }}</td>

                                <td>

                                    <form action="{{url('admin/blog/comment/confirm/'.$c->id)}}" method="post" enctype="multipart/form-data">

                                        {{csrf_field()}}

                                    <button type="submit" class="btn btn-raised g-bg-cyan">Confirm</button>

                                    {{--<a href="{{url('admin/blog/comment/delete/'.$c->id)}}">Delete</a>--}}

                                    </form>

                                    <form action="{{url('admin/blog/comment/delete/'.$c->id)}}" method="post" enctype="multipart/form-data">

                                        {{csrf_field()}}

                                    <button type="submit" class="btn btn-raised g-bg-cyan">Delete</button>

                                    </form>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        </div>
        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}