
@extends('admin.layouts.master')
@section('content')

<section class="content">

    <div class="container-fluid">
        <div class="block-header">
            <h2>Ambulance Details</h2>
            <small class="text-muted">Welcome to Swift application</small>
        </div>
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class=" card patient-profile">
                    <img src="assets/images/image-1.jpg" class="img-responsive" alt="">
                </div>
                <div class="card">
                    <div class="header">
                        <h2>About Patient</h2>
                    </div>
                    <div class="body">
                        <strong>Name</strong>
                        <p>Will Smith</p>
                        <strong>Occupation</strong>
                        <p>UI UX Design</p>
                        <strong>Email ID</strong>
                        <p>will.smith@info.com</p>
                        <strong>Phone</strong>
                        <p>+123 456 789</p>
                        <hr>
                        <strong>Address</strong>
                        <address>85 Bay Drive, New Port Richey, FL 34653</address>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active"><a href="#report" data-toggle="tab" aria-expanded="false">Biography</a></li>
                            <li role="presentation" class=""><a href="#timeline" data-toggle="tab" aria-expanded="true">Timeline</a></li>
                            <li role="presentation"><a href="#report-chart" data-toggle="tab" aria-expanded="true">ECG Report</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="report">
                                <div class="wrap-reset">
                                    <div class="mypost-list">
                                        <div class="post-box">
                                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. </p>
                                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. </p>
                                        </div>
                                        <hr>
                                        <div class="post-box">
                                            <h4>General Report</h4>
                                            <div class="body p-l-0 p-r-0">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <div>Blood Pressure</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>Heart Beat</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>Haemoglobin</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>Sugar</div>
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <hr>
                                        <h4>Education</h4>
                                        <ul class="dis">
                                            <li>B.Com from Ski University</li>
                                            <li>In hac habitasse platea dictumst.</li>
                                            <li>In hac habitasse platea dictumst.</li>
                                            <li>Vivamus elementum semper nisi.</li>
                                            <li>Praesent ac sem eget est egestas volutpat.</li>
                                        </ul>
                                        <hr>
                                        <h4>Past Visit History</h4>
                                        <ul class="dis">
                                            <li>Integer tincidunt.</li>
                                            <li>Praesent vestibulum dapibus nibh.</li>
                                            <li>Integer tincidunt.</li>
                                            <li>Praesent vestibulum dapibus nibh.</li>
                                            <li>Integer tincidunt.</li>
                                            <li>Praesent vestibulum dapibus nibh.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="timeline">
                                <div class="timeline-body">
                                    <div class="timeline m-border">
                                        <div class="timeline-item">
                                            <div class="item-content">
                                                <div class="text-small">Just now</div>
                                                <p>Discharge.</p>
                                            </div>
                                        </div>
                                        <div class="timeline-item border-info">
                                            <div class="item-content">
                                                <div class="text-small">11:30</div>
                                                <p>Routine Checkup</p>
                                            </div>
                                        </div>
                                        <div class="timeline-item border-warning border-l">
                                            <div class="item-content">
                                                <div class="text-small">10:30</div>
                                                <p>Operation </p>
                                            </div>
                                        </div>
                                        <div class="timeline-item border-warning">
                                            <div class="item-content">
                                                <div class="text-small">3 days ago</div>
                                                <p>Routine Checkup</p>
                                            </div>
                                        </div>
                                        <div class="timeline-item border-danger">
                                            <div class="item-content">
                                                <div class="text--muted">Thu, 10 Mar</div>
                                                <p>Routine Checkup</p>
                                            </div>
                                        </div>
                                        <div class="timeline-item border-info">
                                            <div class="item-content">
                                                <div class="text-small">Sat, 5 Mar</div>
                                                <p>Routine Checkup</p>
                                            </div>
                                        </div>
                                        <div class="timeline-item border-danger">
                                            <div class="item-content">
                                                <div class="text-small">Sun, 11 Feb</div>
                                                <p>Blood checkup test</p>
                                            </div>
                                        </div>
                                        <div class="timeline-item border-info">
                                            <div class="item-content">
                                                <div class="text-small">Thu, 17 Jan</div>
                                                <p>Admit patient ward no. 21</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="report-chart">
                                <h4>ECG Report</h4>
                                <div class="stats-row">
                                    <div class="stat-item col-blue">
                                        <h6>Pulse</h6> <b>76</b></div>
                                    <div class="stat-item col-blush">
                                        <h6>BP</h6> <b>112</b></div>
                                </div>
                                <div id="real_time_chart" class="flot-chart" style="padding: 0px; position: relative;"><canvas class="flot-base" width="100" height="320" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100px; height: 320px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 33px; top: 319px; left: 8px; text-align: center;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 33px; top: 319px; left: 50px; text-align: center;">50</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 33px; top: 319px; left: 92px; text-align: center;">100</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 312px; left: 1px; text-align: right;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 251px; left: 1px; text-align: right;">20</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 190px; left: 1px; text-align: right;">40</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 130px; left: 1px; text-align: right;">60</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 69px; left: 1px; text-align: right;">80</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 8px; left: 1px; text-align: right;">100</div></div></div><canvas class="flot-overlay" width="100" height="320" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 100px; height: 320px;"></canvas></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
{{--@include('admin.layouts.footer')--}}
