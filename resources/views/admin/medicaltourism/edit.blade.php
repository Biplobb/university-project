
@extends('admin.layouts.master')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>HOspital </h2>
                <small class="text-muted">HOspital information submit form</small>
            </div>
            <div class="row clearfix">
                {!! Form::open(['url' => '/admin/tourism/update/'.$Hospital->id,'enctype'=>'multipart/form-data','method'=>'patch']) !!}
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>HOspital Information</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder=" Hospital" name="hospital" value="{{ $Hospital->hospital }}">
                                        </div>
                                    </div>
                                </div>


                            </div> <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder=" country" name="country" value="{{ $Hospital->country }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>Branch</label>
                                            <input type="text" class="form-control" placeholder="Branch" name="branch" value="{{ $Hospital->branch }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>Address</label>
                                            <input type="text" class="form-control" placeholder="Address" name="address" value="{{ $Hospital->address }}">
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>Overview</label>
                                            <input type="text" class="form-control" placeholder="Overview" name="overview" value="{{ $Hospital->overview }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>Service</label>
                                            <input type="text" class="form-control" placeholder="Service" name="service" value="{{ $Hospital->service }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="procedure" name="procedures" value="{{ $Hospital->procedures }}">
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>photo update</label>
                                            <input type="file" class="form-control" placeholder="photo update" name="photo" value="{{ $Hospital->photo }}">
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <div class="row clearfix">






                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-raised g-bg-cyan">update</button>
                                    <button type="submit" class="btn btn-raised">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>


        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

