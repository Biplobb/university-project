{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>Appointment trash</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @if(session('permanentdelete'))
                        <div class="alert alert-danger"><storn>{{session('permanentdelete')}} !!</storn></div>
                    @endif
                    <div class="card">
                        <div class="body">
                            <div class="member-card verified">
                                <ul class="header-dropdown">
                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="zmdi zmdi-more-vert"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Edit</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Delete</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Block</a></li>
                                        </ul>
                                    </li>
                                </ul>


                                <div class="">

                                    <table class="table" id="apptdatatable">
                                        <thead>

                                        <tr>
                                            <th scope="col">Sl.</th>
                                            <th scope="col">Patient name</th>
                                            <th scope="col">Phone no.</th>
                                            <th scope="col">Address</th>
                                            <th scope="col">Doctor name</th>
                                            <th scope="col">Hospital name</th>
                                            <th scope="col">Day</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Time</th>
                                            <th scope="col">Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i=1;
                                        @endphp
                                        @foreach($data as $d)
                                            <tr>
                                                <td >{{$i++}}</td>
                                                <td >{{$d->name}}</td>
                                                <td >{{$d->phone}}</td>
                                                <td >{{$d->address}}</td>
                                                <td>{{ $d->doctor_name}}</td>
                                                <td>{{ $d->hospital_name}}</td>
                                                <td>{{ $d->day}}</td>
                                                <td>{{ $d->date}}</td>
                                                <td>{{ $d->time}}</td>
                                                @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->hasRole('superadmin'))
                                                    <td><button type="button" class="btn  btn-raised bg-red waves-effect" id="delete" data-id="{{$d->apptid}}" data-toggle="modal" data-target="#deletemodal"> Delete permanently </button></td>
                                                @else
                                                    <td>Not allowed!</td>
                                                @endif
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section>


    <div class="modal fade Modal-Col-Pink"  id="deletemodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body"> <strong><p>Do you want to confirm this appointment? </p></strong> </div>
                <div class="modal-footer">
                    <form action="{{route('permanentdelete')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="apptid" id="apptid">
                        <button type="button" id="close" class="btn btn-raised  btn-success waves-effect" data-dismiss="modal">CLOSE</button>
                        <button type="submit" id="yesconfirm" class="btn btn-raised  btn-danger waves-effectt">Yes Confirm</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="modal fade Modal-Col-Pink"  id="deletemodal" tabindex="-1" role="dialog">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<h4 class="modal-title" id="defaultModalLabel">Alert !!</h4>--}}
                {{--</div>--}}
                {{--<div class="modal-body"> <strong><p>Do you want to confirm this appointment? </p></strong> </div>--}}
                {{--<div class="modal-footer">--}}
                    {{--<input type="text" name="apptiddelete" id="apptiddelete">--}}
                    {{--<button type="button" id="close" class="btn bg-green waves-effect" data-dismiss="modal">CLOSE</button>--}}
                    {{--<button type="button" id="yesdelete" class="btn  br-red waves-effect">Yes Delete</button>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection

{{--@include('admin.layouts.footer')--}}

@section('script')
    <script>

        $(document).on('click','#delete',function () {
            $('#apptid').val($(this).data('id'));
        });

    </script>
@endsection
