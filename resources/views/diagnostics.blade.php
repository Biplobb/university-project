@extends('layouts.master')
@section('title', 'Deshidoctor | Diagonostics')
@section('content')
    <section id="breadcrumb" class="light-overlay" data-stellar-background-ratio="0.4" style="background-position: 50% 0%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bread-block animate-in move-up animated">
                    <h2>All Bangladeshi Hospitals</h2>
                    <p>Affordable Treatments, Honest &amp; Experienced Dentists.</p>
                </div>
            </div>
        </div>
    </section>
    <section id="blog" class="space v1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 no-padding blog-base">

                    @foreach($diagnostics as $diagnostics)

                        <div class="col-sm-3 blog-block animate-in move-up">
                            <div class="inner">
                                <img src="{{ asset('image/diagnostic-photo/'.$diagnostics->photo) }}" alt="Columba">

                                <a class="hover" href="{{ url('singlediagnosticshow/'.$diagnostics->id)  }}">

                                    <div class="inner">
                                        <h4>{{ $diagnostics->name }}</h4>
                                        <div class="date">{{ $diagnostics->name }}</div>
                                    </div>

                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <article class="col-sm-12 text-center load-more animate-in move-up">
                    <a href="{{ url('diagnostics') }}" class="btn">Load more</a>
                </article>
            </div>
        </div>
    </section>
@endsection