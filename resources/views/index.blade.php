@extends('layouts.master')
@section('title', 'Deshidoctor | Home')
@section('content')
    <!--Banner Content
v5 center light-overlay
style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0,0,0,.5);"
    -->
    <section id="main-banner" class="v5 center light-overlay">
        <!-- <div style="position: relative; width: 100%;">
            <img class="img-responsive" src="{{ asset('front-end/assets/images/banner.jpg') }}" alt="">
        </div> -->
        <div class="container" >
            <div class="row">
                <div class="banner-table">
                    <div class="banner-block text-center">
                        <div class="row searcharea">
                            <div class="col-md-8 col-md-offset-2" style="margin-bottom: 30px">
                                <form id="search" method="post" onsubmit="return validdata()">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="gutter">
                                               <select name="area" id="area" class="form-control" onchange="enable_lookingfor()">
                                                    <option value="">Search Area</option>
                                                    <option value="Dhaka">Dhaka</option>
                                                    <option value="Narayonganj">Narayganj</option>
                                                    <option value="Savar">Savar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="gutter">
                                                <select name="lookingfor" id="lookingfor" class="form-control" onchange="enable_keyword()">
                                                    <option value="">Looking for</option>
                                                    <option value="doctor">Doctor</option>
                                                    <option value="blood">Blood</option>
                                                    <option value="hospital">Hospital</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="gutter">
                                                <input id="keyword" class="form-control" name="keyword" type="text" placeholder="Type Keyword"  autocomplete="off">
                                                <input type="hidden" name="idnt" id="idnt">
                                                <div class="suggestion">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="gutter">
                                                <div class="search-icon">
                                                    <button id="dosearch"> <span class="fa fa-search" ></span></button>
                                                    {{--<button id="dosearch"> <span class="fa fa-search" ></span></button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <h2>Your Health We Care</h2>
                            </div>
                        </div>
                        <ul class="services">
                            <li>
                                <a href="{{route('alldoctors')}}">
                                    <i class="icofont icofont-doctor"></i>
                                    <p>Doctor</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/hospitals')}}">
                                    <i class="icofont icofont-hospital"></i>
                                    <p>Hospital</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('medicaltourism') }}">
                                    <i class="icofont icofont-medical-sign-alt"></i>
                                    <p>Tourism</p>
                                </a>
                            </li>
                             <li>
                                <a href="javaScript:void(0)">
                                    <i class="icofont icofont-cart"></i>
                                    <p>Medical Shop</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('diagnostics') }}">
                                    <i class="icofont icofont-read-book"></i>
                                    <p>Blog</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Offers Area-->
    <section id="our-team" class="space bg-color v5">
        <div class="container">
            <div class="col-sm-6 col-sm-offset-3 text-center main-heading">
                <h2>Exclusive Offers</h2>
              </div>
            <div class="row">
                <div class="col-sm-3 team-block">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{ asset('front-end/assets/images/off-04.jpg') }}" alt="Columba">
                            <div class="hover center"></div>
                        </div>
                        <div class="team-text no-padding">
                            <div class="name">Bangladesh Specialized Hospital</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 team-block">
                    <div class="inner">
                        <div class="user-img">
                            <img class="img-responsive" src="{{ asset('front-end/assets/images/off-01.jpg') }}" alt="Columba">
                            <div class="hover center"></div>
                        </div>
                        <div class="team-text no-padding">
                            <div class="name">Desh Clinic & Diagnostic Center</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 team-block">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{ asset('front-end/assets/images/off-02.jpg') }}" alt="Columba">
                            <div class="hover center"></div>
                        </div>
                        <div class="team-text no-padding">
                            <div class="name">Dr. Kadir haque</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 team-block">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{ asset('front-end/assets/images/off-03.jpg') }}" alt="Columba">
                            <div class="hover center"></div>
                        </div>
                        <div class="team-text no-padding">
                            <div class="name">Medical Ear Thermometer</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Testimonials-->
    <section id="testimonials" class="space v1">
        <div class="container">
            <div class="col-sm-6 col-sm-offset-3 text-center main-heading animate-in move-up">
                <h2>People Say About Us</h2>
                <p>Proin viverra, purus at bibendum molestie, lorem mi dignissim mauris.</p>
            </div>
            <div class="row">
                <div class="col-sm-3 testi-block">
                    <div class="inner">
                        <div class="testi-image text-center">
                            <img src="{{ asset('front-end/assets/images/testimonial01.png') }}" alt="Columba" class="user-image">
                            <img src="{{ asset('front-end/assets/images/testi-icon.png') }}" alt="icon" class="icon">
                        </div>
                        <div class="testi-text">
                            <p>The best way to predict the future is to create it.” – Peter Drucker. I think Deshi doctor is one of the example of this quote. It’s innovative and helpful for us.</p>
                            <div class="name">A K M Monirul Karim</div>
                            <div class="designation">Head of External Affairs <br /> bKash Ltd</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 testi-block">
                    <div class="inner">
                        <div class="testi-image text-center">
                            <img src="{{ asset('front-end/assets/images/testimonial02.png') }}" alt="Columba" class="user-image">
                            <img src="{{ asset('front-end/assets/images/testi-icon.png') }}" alt="icon" class="icon">
                        </div>
                        <div class="testi-text">
                            <p>Cras nibh diam, semper eu bibendum id,  Suspendisse vel orciviverra.</p>
                            <div class="name">Perwez Anzam Moonir</div>
                            <div class="designation">Joint Director  <br /> Bangladesh Bank</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 testi-block">
                    <div class="inner">
                        <div class="testi-image text-center">
                            <img src="{{ asset('front-end/assets/images/testimonial03.png') }}" alt="Columba" class="user-image">
                            <img src="{{ asset('front-end/assets/images/testi-icon.png') }}" alt="icon" class="icon">
                        </div>
                        <div class="testi-text">
                            <p>Cras nibh diam, semper eu bibendum vel orci eu mauris consectetur viverra.</p>
                            <div class="name">Sabiha Neepa</div>
                            <div class="designation">Joint News Editor <br /> Somoy Media Limited (24X7 News Channel)</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 testi-block">
                    <div class="inner">
                        <div class="testi-image text-center">
                            <img src="{{ asset('front-end/assets/images/testimonial04.png') }}" alt="Columba" class="user-image">
                            <img src="{{ asset('front-end/assets/images/testi-icon.png') }}" alt="icon" class="icon">
                        </div>
                        <div class="testi-text">
                            <p>Cras nibh diam, semper Suspendisse vel orci eu mauris consectetur viverra.</p>
                            <div class="name">Abdur Razzaque</div>
                            <div class="designation">Former Joint Director <br /> Govt of the People's Republic of Bangladesh, Employees</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 text-center load-more">
                    <a href="javaScript:void(0)" class="btn btn-small">Testimonials</a>
                </div>
            </div>
        </div>
    </section>
    <!--Client Logo-->
    <section id="client-logo" class="space bg-color">
        <div class="container">
            <div class="row">
                <div class="text-center main-heading animate-in move-up animated">
                    <h2>Our Valueable Clients</h2>
                </div>
                <div class="col-sm-12 no-padding owl-carousel owl-theme logo-slider">
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients1.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients5.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients3.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients4.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients2.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients1.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients2.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients3.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients4.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients5.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients3.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients4.png')}}"></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-sm-12">
                            <a href="javaScript:void(0)"><img src="{{asset('front-end/assets/images/valuable-clients1.png')}}"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        var token='{{\Illuminate\Support\Facades\Session::token()}}';
    </script>
    <script src="{{asset('front-end/assets/js/myjs.js')}}"></script>
@endsection