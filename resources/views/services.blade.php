@extends('layouts.master')

@section('content')
    <section id="breadcrumb" class="space light-overlay" data-stellar-background-ratio="0.4">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bread-block animate-in move-up">
                    <h2>Services 01</h2>
                    <p>Affordable Treatments, Honest & Experienced Dentists.</p>
                </div>
                <div class="col-sm-6 bread-block text-right animate-in move-up">
                    <a href="#" class="btn">Book appointment</a>
                </div>
            </div>
        </div>
    </section>
    <!--Services-->
    <section id="services" class="space v4">
        <div class="container">
            <div class="row">
                <div class="service-block col-sm-4 animate-in move-up">
                    <div class="inner">
                        <img src="{{asset('front-end/assets/images/service-v4-1.jpg')}}" alt="Columba">
                        <h4><a href="#">X- Ray</a> </h4>
                        <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut
                            <br> dapibus lacus </p>
                        <a href="#" class="simple">Read More</a>
                    </div>
                </div>
                <div class="service-block col-sm-4 animate-in move-up">
                    <div class="inner">
                        <img src="{{asset('front-end/assets/images/service-v4-1.jpg')}}" alt="Columba">
                        <h4><a href="#">Traumatology</a> </h4>
                        <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut
                            <br> dapibus lacus </p>
                        <a href="#" class="simple">Read More</a>
                    </div>
                </div>
                <div class="service-block col-sm-4 animate-in move-up">
                    <div class="inner">
                        <img src="{{asset('front-end/assets/images/service-v4-1.jpg')}}" alt="Columba">
                        <h4><a href="#">Pregnancy</a> </h4>
                        <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut
                            <br> dapibus lacus </p>
                        <a href="#" class="simple">Read More</a>
                    </div>
                </div>
                <div class="service-block col-sm-4 animate-in move-up">
                    <div class="inner">
                        <img src="{{asset('front-end/assets/images/service-v4-1.jpg')}}" alt="Columba">
                        <h4><a href="#">Births</a> </h4>
                        <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut
                            <br> dapibus lacus </p>
                        <a href="#" class="simple">Read More</a>
                    </div>
                </div>
                <div class="service-block col-sm-4 animate-in move-up">
                    <div class="inner">
                        <img src="{{asset('front-end/assets/images/service-v4-1.jpg')}}" alt="Columba">
                        <h4><a href="#">Dental</a> </h4>
                        <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut
                            <br> dapibus lacus </p>
                        <a href="#" class="simple">Read More</a>
                    </div>
                </div>
                <div class="service-block col-sm-4 animate-in move-up">
                    <div class="inner">
                        <img src="{{asset('front-end/assets/images/service-v4-1.jpg')}}" alt="Columba">
                        <h4><a href="#">Our Research</a> </h4>
                        <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut
                            <br> dapibus lacus </p>
                        <a href="#" class="simple">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--action-->
    <section class="action-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 ">
                    <div class="col-sm-8 animate-in move-up">
                        <h2>Planning A Visit To The Doctor</h2>
                    </div>
                    <div class="col-sm-4 animate-in move-up">
                        <a href="#" class="btn">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection