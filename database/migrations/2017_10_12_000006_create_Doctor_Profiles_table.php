<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Doctor_Profiles', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('name','25');
            $table->string('email','50')->unique();
            $table->string('password');
            $table->string('speciality','25');
            $table->string('degree','150');
            $table->string('phone','25');
            $table->string('address','25');
            $table->string('member','25');
            $table->string('awards','150');
            $table->string('experience','150');
            $table->string('photo','150');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Doctor_Profiles');
    }
}
