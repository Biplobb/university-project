<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeigndoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foreigndoctor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('qualification');
            $table->string('speciality');
            $table->string('overview');
            $table->string('photo');

            $table->string('email','50')->unique();
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreigndoctor');
    }
}
