<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeigndoctorMedicaltourismTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foreigndoctor_medicaltourism', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('foreigndoctor_id')->nullable();
            $table->integer('medicaltourism_id')->unsigned();
            $table->string('assigned');

            $table->integer('first_fees')->nullable();
            $table->integer('second_fees')->nullable();

            $table->foreign('foreigndoctor_id')->references('id')->on('foreigndoctor')->onDelete('cascade');
            $table->foreign('medicaltourism_id')->references('id')->on('medicaltourism')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreigndoctor_medicaltourism');
    }
}
