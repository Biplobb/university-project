<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Hospital', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('location',255);
            $table->string('service',3000);
            $table->string('about',3000);
            $table->string('photo');
            $table->string('contact');
            $table->string('review',3000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Hospital');
    }
}
