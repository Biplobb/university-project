<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestorderPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testorder_price', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('testorder_id')->unsigned();

            $table->foreign('testorder_id')->references('id')->on('testorder');

            $table->string('previous');
            $table->string('discount');
            $table->string('discountprice');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testorder_price');
    }
}
