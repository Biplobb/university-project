<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class HospitalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $hospitals = [
            [
                'name'       => 'Square',
                'location'       => 'Dhanmondi',
                'service'       => 'Heart, Dental, Cardiology',
                'about'       => 'Good',
                'photo'       => '280467.jpg',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],
            [
                'name'       => 'Lab aid',
                'location'       => 'Dhanmondi',
                'service'       => 'Heart, Dental, Cardiology',
                'about'       => 'Good',
                'photo'       => '280467.jpg',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],
            [
                'name'       => 'Popular',
                'location'       => 'Dhanmondi',
                'service'       => 'Heart, Dental, Cardiology',
                'about'       => 'Good',
                'photo'       => '280467.jpg',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],
            [
                'name'       => 'Ibne Sina',
                'location'       => 'Dhanmondi',
                'service'       => 'Heart, Dental, Cardiology',
                'about'       => 'Good',
                'photo'       => '280467.jpg',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],
        ];
        \Illuminate\Support\Facades\DB::table('hospital')->insert($hospitals);

    }
}
