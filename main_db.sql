-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: 192.168.10.5:3306
-- Generation Time: Feb 01, 2018 at 04:31 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `deshidoctor_maindb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone`, `nid`, `photo`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Support admin', 'supportadmin@gmail.com', '01874680376', '445156484215451', '9043.png', '$2y$10$ZB5CDJ00GJTiSgBnwO0eLO3TBYQ4uRX8jxW.yqbTyRM2dTDkUFvfi', '3TLyhFlfqEy7nvtZjXX9dndv6k1anemzGJnApsIeye6PhqdetvBhihqypBbI', '2018-01-30 13:56:55', '2018-01-30 13:56:55'),
(2, 'Super admin', 'superadmin@gmail.com', '01874680376', '445156484215451', '9043.png', '$2y$10$J36KeAvRhPh8ZQT.4WKP9OArGX/rHjitd94PhYKwWrUljlMWIwQgm', 'GLvLFJHLZZSB54ZDLAVJSyuD91q7m3G9rKCYs10SptNwSqwBZOw9KbT9rSnr', '2018-01-30 13:56:56', '2018-01-30 13:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`id`, `admin_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` int(10) UNSIGNED NOT NULL,
  `doctor_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appointment_round` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `doctor_id`, `hospital_id`, `name`, `address`, `phone`, `email`, `time`, `date`, `day`, `appointment_round`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'd2f9b130-05cd-11e8-b821-45a03bb64b17', 1, 'Nahid', 'Dhaka, Armanitola', '01745917552', 'nahid@gmail.com', '01:40:00', '2018-02-04', 'Sunday', 'new', 'yes', NULL, '2018-01-30 19:58:15', '2018-01-30 19:58:15'),
(2, 'd2f9b130-05cd-11e8-b821-45a03bb64b17', 1, 'Jobbar', 'Dhaka', '54365465234', 'j@gmail.com', '03:40:00', '2018-01-31', 'Wednesday', 'new', 'yes', NULL, '2018-01-30 20:02:53', '2018-01-30 20:02:53'),
(3, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 'Abdul', 'Dhaka', '5436546323', 'abd@gmail.com', '03:40:00', '2018-02-07', 'Wednesday', 'new', 'yes', NULL, '2018-01-30 20:05:56', '2018-01-30 20:05:56'),
(4, 'a99bffe0-05d0-11e8-93dc-db5269c91fae', 5, 'Samuel', 'Dhaka', '54365466546', 'sam@gmail.com', '02:00:00', '2018-02-11', 'Sunday', 'new', 'yes', NULL, '2018-01-30 20:18:30', '2018-01-30 20:18:30'),
(5, 'a99bffe0-05d0-11e8-93dc-db5269c91fae', 5, 'Sam', 'Dhaka', '543654675467', 'nahid76@gmail.com', '03:40:00', '2018-02-07', 'Wednesday', 'new', 'yes', NULL, '2018-01-30 21:15:44', '2018-01-30 21:15:44'),
(6, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 'Nahid', 'Dhaka', '5436546645', 'nahid21@gmail.com', '04:20:00', '2018-02-14', 'Wednesday', 'new', 'not', NULL, '2018-01-30 21:22:31', '2018-01-30 21:22:31'),
(7, 'd2f9b130-05cd-11e8-b821-45a03bb64b17', 1, 'Nahid 65', 'Dhaka', '54365466546', 'nahid@gmail.com', '04:20:00', '2018-01-31', 'Wednesday', 'old', 'not', NULL, '2018-01-30 22:16:04', '2018-01-30 22:16:04'),
(8, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 4, 'Nahid', 'Dhaka', '54365465435', 'nahid@gmail.com', '03:00:00', '2018-01-31', 'Wednesday', 'new', 'not', NULL, '2018-01-30 22:35:43', '2018-01-30 22:35:43'),
(9, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 5, 'Nahid', 'Dhaka', '543654657090', 'nahid@gmail.com', '04:30:00', '2018-02-11', 'Sunday', 'new', 'not', '2018-01-31 07:56:35', '2018-01-30 22:36:52', '2018-01-31 07:56:35'),
(10, '67dd3c60-05e4-11e8-aa56-ab6bfeb2922a', 6, 'Jobbar', 'Dhaka', '5698745', 'nahid45@gmail.com', '03:40:00', '2018-02-15', 'Thursday', 'new', 'yes', NULL, '2018-01-30 22:39:48', '2018-01-30 22:39:48'),
(11, 'd2f9b130-05cd-11e8-b821-45a03bb64b17', 1, 'Nahid', 'Dhaka', '8743649', 's@gmail.com', '02:40:pm', '2018-02-14', 'Wednesday', 'new', 'not', NULL, '2018-01-31 10:56:38', '2018-01-31 10:56:38'),
(12, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 'Nahid', 'Dhaka', '84274467464567', 'nahid@gmail.com', '04:00:pm', '2018-02-07', 'Wednesday', 'new', 'not', NULL, '2018-01-31 10:57:28', '2018-01-31 10:57:28'),
(13, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 4, 'Sam', 'Dhaka', '8678567456', 's@gmail.com', '06:00:pm', '2018-02-08', 'Thursday', 'new', 'not', NULL, '2018-01-31 10:58:50', '2018-01-31 10:58:50'),
(14, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 'Nahid', 'Dhaka', '543654698646', 'nahid@gmail.com', '04:00:pm', '2018-02-14', 'Wednesday', 'new', 'not', NULL, '2018-01-31 11:04:39', '2018-01-31 11:04:39'),
(15, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 'Jobbar 21', 'Dhaka', '5436546745903', 'nahid@gmail.com', '02:20:pm', '2018-02-17', 'Saturday', 'new', 'not', NULL, '2018-01-31 11:06:08', '2018-01-31 11:06:08'),
(16, '67dd3c60-05e4-11e8-aa56-ab6bfeb2922a', 6, 'Sam 543', 'Dhaka', '543654609431', 'nahid@gmail.com', '04:20:pm', '2018-02-15', 'Thursday', 'new', 'not', NULL, '2018-01-31 11:08:58', '2018-01-31 11:08:58'),
(17, '8c12f350-064d-11e8-b818-2553cffac2d6', 6, 'Sam65', 'Dhaka', '54365467354345', 'nahid@gmail.com', '02:40:pm', '2018-02-14', 'Wednesday', 'new', 'not', NULL, '2018-01-31 11:12:07', '2018-01-31 11:12:07'),
(18, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 4, 'Jobbar9789', 'Dhaka', '543654667567', 'nahid@gmail.com', '04:20:pm', '2018-02-14', 'Wednesday', 'new', 'not', NULL, '2018-01-31 11:15:41', '2018-01-31 11:15:41'),
(19, '9d156fa0-064e-11e8-adbd-bdc6d70d8324', 7, 'Nahid 78', 'Dhaka', '54365465345', 'nahid@gmail.com', '04:00:pm', '2018-02-09', 'Friday', 'new', 'not', NULL, '2018-01-31 11:21:09', '2018-01-31 11:21:09'),
(20, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 'Jobbar', 'Dhaka', '543654694374', 's@gmail.com', '02:20:pm', '2018-02-10', 'Saturday', 'new', 'not', NULL, '2018-01-31 11:56:32', '2018-01-31 11:56:32'),
(21, 'a99bffe0-05d0-11e8-93dc-db5269c91fae', 5, 'Nahid', 'Dhaka', '54365468554', 'nahid@gmail.com', '02:20:pm', '2018-02-14', 'Wednesday', 'new', 'yes', NULL, '2018-01-31 12:30:17', '2018-01-31 12:30:17'),
(22, 'a99bffe0-05d0-11e8-93dc-db5269c91fae', 5, 'Nahid', 'Dhaka', '523452345767', 'k@gmail.com', '03:00:pm', '2018-02-14', 'Wednesday', 'new', 'not', '2018-01-31 22:29:32', '2018-01-31 12:41:16', '2018-01-31 22:29:32'),
(23, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 'nahid', 'dhaka', '74685358', '1000163@daffodil.ac', '04:00:pm', '2018-02-14', 'Wednesday', 'new', 'not', NULL, '2018-01-31 16:20:12', '2018-01-31 16:20:12'),
(24, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 4, 'babul', 'dhaka', '74754674', '1000163@daffodil.ac', '03:00:pm', '2018-02-11', 'Sunday', 'new', 'not', NULL, '2018-01-31 17:45:45', '2018-01-31 17:45:45'),
(25, 'd2f9b130-05cd-11e8-b821-45a03bb64b17', 1, 'sam', 'dhaka', '657437456', 'z@gmail.com', '02:20:pm', '2018-02-14', 'Wednesday', 'new', 'yes', NULL, '2018-01-31 17:47:38', '2018-01-31 17:47:38'),
(26, '67dd3c60-05e4-11e8-aa56-ab6bfeb2922a', 6, 'Nahid', 'Dhaka', '54365467567', 'nahid@gmail.com', '03:00:pm', '2018-02-15', 'Thursday', 'new', 'not', NULL, '2018-01-31 22:00:27', '2018-01-31 22:00:27'),
(27, 'a99bffe0-05d0-11e8-93dc-db5269c91fae', 5, 'Jobbar', 'Dhaka', '5436546', 'nahid@gmail.com', '03:00:pm', '2018-02-14', 'Wednesday', 'new', 'not', NULL, '2018-01-31 22:30:55', '2018-01-31 22:30:55'),
(28, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 'Nahid', 'Dhaka', '5436546545121', 'nahid@gmail.com', '03:40:pm', '2018-01-31', 'Wednesday', 'new', 'not', NULL, '2018-01-31 22:37:57', '2018-01-31 22:37:57'),
(29, 'd2f9b130-05cd-11e8-b821-45a03bb64b17', 1, 'Nahid', 'Dhaka', '543654697087', 'nahid@gmail.com', '02:40:pm', '2018-02-14', 'Wednesday', 'new', 'not', NULL, '2018-01-31 23:08:09', '2018-01-31 23:08:09'),
(30, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 'Nahid', 'Dhaka', '5436546654', 'nahid@gmail.com', '02:40:pm', '2018-02-10', 'Saturday', 'new', 'yes', NULL, '2018-01-31 23:17:04', '2018-01-31 23:17:04'),
(31, '67dd3c60-05e4-11e8-aa56-ab6bfeb2922a', 6, 'fozlu', '01,bolas,demra', '0171254545', 'combo@gmailcom', '05:00:pm', '2018-02-06', 'Tuesday', 'new', 'yes', NULL, '2018-01-31 23:25:56', '2018-01-31 23:25:56'),
(32, 'd2f9b130-05cd-11e8-b821-45a03bb64b17', 1, 'biplob', 'dhaka', '01714740719', 'nbiplob15@gmail.com', '03:40:pm', '2018-02-07', 'Wednesday', 'new', 'yes', NULL, '2018-02-01 15:31:45', '2018-02-01 15:31:45'),
(33, '8c12f350-064d-11e8-b818-2553cffac2d6', 6, 'Nahid', 'Dhaka', '6357951', 's@gmail.com', '02:20:pm', '2018-02-14', 'Wednesday', 'new', 'not', NULL, '2018-02-01 15:50:15', '2018-02-01 15:50:15'),
(34, '8c12f350-064d-11e8-b818-2553cffac2d6', 6, 'Nahid', 'Dhaka', '658793241258', 's@gmail.com', '02:20:pm', '2018-02-07', 'Wednesday', 'new', 'not', NULL, '2018-02-01 15:50:35', '2018-02-01 15:50:35'),
(35, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 4, 'Nahid', 'Dhaka', '5436546756', 'nahid@gmail.com', '02:20:pm', '2018-02-07', 'Wednesday', 'new', 'not', NULL, '2018-02-01 21:16:42', '2018-02-01 21:16:42');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_post` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doctor_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `subtitle`, `author`, `blog_post`, `type`, `image`, `doctor_id`, `created_at`, `updated_at`) VALUES
(1, 'বয়স কম রাখতে চান? দেখে নিন টমেটোর জাদু', 'This is subtitle', 'Deshidoctor', 'ইউরোপে টমেটোর গুণ সম্পর্কে বলা হয়, ‘টমেটো যদি লাল হয়, চিকিৎসকের মুখ হয় নীল।’ অর্থাৎ নিয়মিত টমেটো খেলে চিকিৎসকের কাছে যাওয়ার দরকার হয় না। এখনকার কর্মব্যস্ত জীবনে যেকোনো সময় মানুষের জীবনে পেয়ে বসে অবসাদ, বিষণ্নতা। টমেটো খেলে এই অবসাদ থেকে মুক্তি পাওয়া যায়।\r\n\r\nটমেটোর গুণের কথা সবারই জানা। এক কাপ বা ১৮৯ গ্রাম টমেটোতে আছে ৩৮ শতাংশ ভিটামিন সি, ৩০ শতাংশ ভিটামিন এ, ১৮ শতাংশ ভিটামিন কে, ১৩ শতাংশ পটাশিয়াম ও ১০ শতাংশ ম্যাঙ্গানিজ। এ ছাড়াও আছে ভিটামিন ই, লৌহ, ফলেট ও আঁশ। এত গুণের কারণে এই মৌসুমে প্রতিদিন সালাদের সঙ্গে টমেটো খেতে পারেন। এ ছাড়াও টমেটোর এমন কিছু গুণ আছে যা বুড়িয়ে যাওয়া ঠেকাতে পারে। শরীরকে রোগ প্রতিরোধক্ষম করে তোলে টমেটো। সাম্প্রতিক এক গবেষণায় টমেটোর এসব কার্যক্ষমতার কথা বলা হয়েছে। গবেষকেরা বলেছেন, টমেটোতে আছে দারুণ অ্যান্টি-অক্সিডেটিভ প্রভাব, যা কোষকে বুড়ো হতে দেয় না। এ ছাড় নানা রকম ক্যানসার প্রতিরোধ করতে পারে টমেটো।', 'Featured', '3881058.jpg', 'd2f9b130-05cd-11e8-b821-45a03bb64b17', '2018-01-30 20:17:42', '2018-01-30 20:17:42'),
(2, 'চুল পড়ার আসল কারণগুলো জানুন !!', 'subtitle here', 'deshidoctor', 'আমাদের প্রতিটা অঙ্গপ্রত্যঙ্গের মধ্যে চুল hair খুবই প্রিয়। যাকে সুস্থ রাখার জন্য যথেষ্ট যত্ন নেওয়া প্রয়োজন। কিন্তু চুলের সমস্যা hair problem প্রায়ই আমাদের প্রত্যেকের থাকে।\r\n\r\nচুল পড়া, Hair Fall টাক পড়ে যাওয়া, চুল অকালে পেকে যাওয়া প্রভৃতি বিভিন্ন সমস্যায় আমাদের পড়তে হয়। প্রত্যেকদিন যদি ১০০টি করে চুল পড়ে, Hair Fall তাহলে তা স্বাভাবিক। কিন্তু কখনও যদি এমনটা মনে হয় যে, স্বাভাবিকের তুলনায় আপনার বেশি চুল পড়ছে, তাহলে তার জন্য অবশ্যই চিকিৎসা করান।\r\n\r\nচুল পড়ার Hair Fall বিভিন্ন কারণ হতে পারে। চিকিৎসা শুরু করার আগে জেনে নিন কোন কোন কারণে চুলের সমস্যা হয় –\r\n\r\n১) সঠিকভাবে চুল আঁচড়ানোটা খুবই জরুরি। চুল পড়ে যাওয়ায় অধিকাংশ ক্ষেত্রে দায়ী থাকে সঠিকভাবে চুল না আঁচড়ানো। যেমন, ভেজা চুল কখনওই আঁচড়ানো উচিৎ নয়। ভেজা অবস্থায় চুলের গোড়া নরম থাকে। সেই সময় চুল আঁচড়ালে চুল ছিঁড়ে যাওয়ার সম্ভাবনা বেশি থাকে।\r\n২) গরম জল চুলের পক্ষে খুবই অপকারী। কখনওই চুল গরম জল দিয়ে ধোবেন না। চেষ্টা করবেন ঠান্ডা জলে চুল ধুতে।\r\n৩) জিনগত দিক থেকেও চুলের বিভিন্ন সমস্যা হতে পারে। শুধু চুল নয়, আমাদের শরীরের বিভিন্ন দিকে প্রভাব ফেলে আমাদের জিন। তাই আপনার পরিবারের কোনও সদস্যের যদি চুলের কোনও সমস্যা থেকে থাকে, তাহলে আপনারও হতে পারে।\r\n৪) চুলে অত্যধিক পরিমাণে কেমিক্যাল জাতীয় প্রোডাক্ট ব্যবহার কিংবা সারাক্ষণ স্টাইলিং কিট ব্যবহারও চুলের সমস্যার অন্যতম কারণ হতে পারে।\r\n৫) আমাদের খাবারের ধরনও আমাদের চুলের সমস্যার Hair Problem কারণ হতে পারে। চুলের সমস্যাকে রোধ করতে আমাদের ডায়েটের দিকেও নজর দেওয়া দরকার।\r\n৬) ৫০ কিংবা ৬০ বছরের উর্ধ্বের ব্যক্তিদের চুল পড়ার সমস্যা খুবই সাধারণ। বয়সজনিত কারণে চুল, ত্বক, নখের বৃদ্ধি হ্রাস হয়।', 'Regular', '2598084.jpg', 'a99bffe0-05d0-11e8-93dc-db5269c91fae', '2018-01-30 20:29:32', '2018-01-30 20:29:32');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `comments` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(10) UNSIGNED NOT NULL,
  `day_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `day_name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'Saturday', 6, '2018-01-30 13:57:35', '2018-01-30 13:57:35'),
(2, 'Sunday', 0, '2018-01-30 13:57:35', '2018-01-30 13:57:35'),
(3, 'Monday', 1, '2018-01-30 13:57:35', '2018-01-30 13:57:35'),
(4, 'Tuesday', 2, '2018-01-30 13:57:35', '2018-01-30 13:57:35'),
(5, 'Wednesday', 3, '2018-01-30 13:57:35', '2018-01-30 13:57:35'),
(6, 'Thursday', 4, '2018-01-30 13:57:35', '2018-01-30 13:57:35'),
(7, 'Friday', 5, '2018-01-30 13:57:35', '2018-01-30 13:57:35');

-- --------------------------------------------------------

--
-- Table structure for table `dcotor_schedules`
--

CREATE TABLE `dcotor_schedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `doctor_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital_id` int(10) UNSIGNED NOT NULL,
  `day_id` int(10) UNSIGNED NOT NULL,
  `interval` int(11) NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `available` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dcotor_schedules`
--

INSERT INTO `dcotor_schedules` (`id`, `doctor_id`, `hospital_id`, `day_id`, `interval`, `start`, `end`, `available`, `created_at`, `updated_at`) VALUES
(1, 'd2f9b130-05cd-11e8-b821-45a03bb64b17', 1, 2, 20, '13:00:00', '16:00:00', 'yes', '2018-01-30 19:57:23', '2018-01-30 19:57:23'),
(2, 'd2f9b130-05cd-11e8-b821-45a03bb64b17', 1, 5, 20, '13:00:00', '17:00:00', 'yes', '2018-01-30 19:57:23', '2018-01-30 19:57:23'),
(3, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 1, 20, '13:00:00', '16:00:00', 'yes', '2018-01-30 20:05:14', '2018-01-30 20:05:14'),
(4, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 3, 20, '13:00:00', '16:00:00', 'yes', '2018-01-30 20:05:14', '2018-01-30 20:05:14'),
(5, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 5, 20, '15:00:00', '17:00:00', 'yes', '2018-01-30 20:05:14', '2018-01-30 20:05:14'),
(6, 'a99bffe0-05d0-11e8-93dc-db5269c91fae', 5, 2, 30, '13:00:00', '16:00:00', 'yes', '2018-01-30 20:17:52', '2018-01-30 20:17:52'),
(7, 'a99bffe0-05d0-11e8-93dc-db5269c91fae', 5, 5, 20, '13:00:00', '17:00:00', 'yes', '2018-01-30 20:17:52', '2018-01-30 20:17:52'),
(8, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 4, 2, 30, '13:00:00', '16:00:00', 'yes', '2018-01-30 22:34:33', '2018-01-30 22:34:33'),
(9, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 4, 5, 20, '13:00:00', '17:00:00', 'yes', '2018-01-30 22:34:33', '2018-01-30 22:34:33'),
(10, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 4, 6, 20, '13:00:00', '18:00:00', 'yes', '2018-01-30 22:34:33', '2018-01-30 22:34:33'),
(11, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 5, 2, 30, '13:00:00', '19:00:00', 'yes', '2018-01-30 22:34:58', '2018-01-30 22:34:58'),
(12, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 5, 5, 30, '13:00:00', '17:00:00', 'yes', '2018-01-30 22:34:58', '2018-01-30 22:34:58'),
(13, '67dd3c60-05e4-11e8-aa56-ab6bfeb2922a', 6, 4, 20, '13:00:00', '18:00:00', 'yes', '2018-01-30 22:39:08', '2018-01-30 22:39:08'),
(14, '67dd3c60-05e4-11e8-aa56-ab6bfeb2922a', 6, 6, 20, '14:00:00', '17:00:00', 'yes', '2018-01-30 22:39:08', '2018-01-30 22:39:08'),
(15, '8c12f350-064d-11e8-b818-2553cffac2d6', 6, 3, 20, '13:00:00', '16:00:00', 'yes', '2018-01-31 11:11:40', '2018-01-31 11:11:40'),
(16, '8c12f350-064d-11e8-b818-2553cffac2d6', 6, 5, 20, '13:00:00', '16:00:00', 'yes', '2018-01-31 11:11:40', '2018-01-31 11:11:40'),
(17, '9d156fa0-064e-11e8-adbd-bdc6d70d8324', 7, 3, 30, '13:00:00', '17:00:00', 'yes', '2018-01-31 11:19:22', '2018-01-31 11:19:22'),
(18, '9d156fa0-064e-11e8-adbd-bdc6d70d8324', 7, 5, 20, '13:00:00', '16:00:00', 'yes', '2018-01-31 11:19:22', '2018-01-31 11:19:22'),
(19, '9d156fa0-064e-11e8-adbd-bdc6d70d8324', 7, 7, 30, '14:00:00', '17:00:00', 'yes', '2018-01-31 11:19:22', '2018-01-31 11:19:22');

-- --------------------------------------------------------

--
-- Table structure for table `degree`
--

CREATE TABLE `degree` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diagnostic_center`
--

CREATE TABLE `diagnostic_center` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `diagnostic_center`
--

INSERT INTO `diagnostic_center` (`id`, `name`, `location`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Aims Diagnostic & Medical Center', '57, Pilkhana Road, Azimpur, Dhaka-1205', '633187.png', '2018-01-31 10:06:45', '2018-01-31 10:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `diagnostic_center_tests`
--

CREATE TABLE `diagnostic_center_tests` (
  `id` int(10) UNSIGNED NOT NULL,
  `Tests_id` int(10) UNSIGNED NOT NULL,
  `Diagnostic_Center_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `diagnostic_center_tests`
--

INSERT INTO `diagnostic_center_tests` (`id`, `Tests_id`, `Diagnostic_Center_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-01-31 10:07:48', '2018-01-31 10:07:48'),
(2, 2, 1, '2018-01-31 10:07:48', '2018-01-31 10:07:48'),
(3, 3, 1, '2018-01-31 10:07:48', '2018-01-31 10:07:48');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(2) UNSIGNED NOT NULL,
  `division_id` int(2) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `bn_name` varchar(50) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `division_id`, `name`, `bn_name`, `lat`, `lon`, `website`, `created_at`, `updated_at`) VALUES
(1, 3, 'Dhaka', 'ঢাকা', 23.7115253, 90.4111451, 'www.dhaka.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(2, 3, 'Faridpur', 'ফরিদপুর', 23.6070822, 89.8429406, 'www.faridpur.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(3, 3, 'Gazipur', 'গাজীপুর', 24.0022858, 90.4264283, 'www.gazipur.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(4, 3, 'Gopalganj', 'গোপালগঞ্জ', 23.0050857, 89.8266059, 'www.gopalganj.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(5, 8, 'Jamalpur', 'জামালপুর', 24.937533, 89.937775, 'www.jamalpur.gov.bd', '2015-09-13 14:33:27', '2016-04-06 20:48:38'),
(6, 3, 'Kishoreganj', 'কিশোরগঞ্জ', 24.444937, 90.776575, 'www.kishoreganj.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(7, 3, 'Madaripur', 'মাদারীপুর', 23.164102, 90.1896805, 'www.madaripur.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(8, 3, 'Manikganj', 'মানিকগঞ্জ', 0, 0, 'www.manikganj.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(9, 3, 'Munshiganj', 'মুন্সিগঞ্জ', 0, 0, 'www.munshiganj.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(10, 8, 'Mymensingh', 'ময়মনসিং', 0, 0, 'www.mymensingh.gov.bd', '2015-09-13 14:33:27', '2016-04-06 20:49:01'),
(11, 3, 'Narayanganj', 'নারায়াণগঞ্জ', 23.63366, 90.496482, 'www.narayanganj.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(12, 3, 'Narsingdi', 'নরসিংদী', 23.932233, 90.71541, 'www.narsingdi.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(13, 8, 'Netrokona', 'নেত্রকোনা', 24.870955, 90.727887, 'www.netrokona.gov.bd', '2015-09-13 14:33:27', '2016-04-06 20:46:31'),
(14, 3, 'Rajbari', 'রাজবাড়ি', 23.7574305, 89.6444665, 'www.rajbari.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(15, 3, 'Shariatpur', 'শরীয়তপুর', 0, 0, 'www.shariatpur.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(16, 8, 'Sherpur', 'শেরপুর', 25.0204933, 90.0152966, 'www.sherpur.gov.bd', '2015-09-13 14:33:27', '2016-04-06 20:48:21'),
(17, 3, 'Tangail', 'টাঙ্গাইল', 0, 0, 'www.tangail.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(18, 5, 'Bogra', 'বগুড়া', 24.8465228, 89.377755, 'www.bogra.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(19, 5, 'Joypurhat', 'জয়পুরহাট', 0, 0, 'www.joypurhat.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(20, 5, 'Naogaon', 'নওগাঁ', 0, 0, 'www.naogaon.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(21, 5, 'Natore', 'নাটোর', 24.420556, 89.000282, 'www.natore.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(22, 5, 'Nawabganj', 'নবাবগঞ্জ', 24.5965034, 88.2775122, 'www.chapainawabganj.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(23, 5, 'Pabna', 'পাবনা', 23.998524, 89.233645, 'www.pabna.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(24, 5, 'Rajshahi', 'রাজশাহী', 0, 0, 'www.rajshahi.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(25, 5, 'Sirajgonj', 'সিরাজগঞ্জ', 24.4533978, 89.7006815, 'www.sirajganj.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(26, 6, 'Dinajpur', 'দিনাজপুর', 25.6217061, 88.6354504, 'www.dinajpur.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(27, 6, 'Gaibandha', 'গাইবান্ধা', 25.328751, 89.528088, 'www.gaibandha.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(28, 6, 'Kurigram', 'কুড়িগ্রাম', 25.805445, 89.636174, 'www.kurigram.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(29, 6, 'Lalmonirhat', 'লালমনিরহাট', 0, 0, 'www.lalmonirhat.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(30, 6, 'Nilphamari', 'নীলফামারী', 25.931794, 88.856006, 'www.nilphamari.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(31, 6, 'Panchagarh', 'পঞ্চগড়', 26.3411, 88.5541606, 'www.panchagarh.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(32, 6, 'Rangpur', 'রংপুর', 25.7558096, 89.244462, 'www.rangpur.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(33, 6, 'Thakurgaon', 'ঠাকুরগাঁও', 26.0336945, 88.4616834, 'www.thakurgaon.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(34, 1, 'Barguna', 'বরগুনা', 0, 0, 'www.barguna.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(35, 1, 'Barisal', 'বরিশাল', 0, 0, 'www.barisal.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(36, 1, 'Bhola', 'ভোলা', 22.685923, 90.648179, 'www.bhola.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(37, 1, 'Jhalokati', 'ঝালকাঠি', 0, 0, 'www.jhalakathi.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(38, 1, 'Patuakhali', 'পটুয়াখালী', 22.3596316, 90.3298712, 'www.patuakhali.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(39, 1, 'Pirojpur', 'পিরোজপুর', 0, 0, 'www.pirojpur.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(40, 2, 'Bandarban', 'বান্দরবান', 22.1953275, 92.2183773, 'www.bandarban.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(41, 2, 'Brahmanbaria', 'ব্রাহ্মণবাড়িয়া', 23.9570904, 91.1119286, 'www.brahmanbaria.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(42, 2, 'Chandpur', 'চাঁদপুর', 23.2332585, 90.6712912, 'www.chandpur.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(43, 2, 'Chittagong', 'চট্টগ্রাম', 22.335109, 91.834073, 'www.chittagong.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(44, 2, 'Comilla', 'কুমিল্লা', 23.4682747, 91.1788135, 'www.comilla.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(45, 2, 'Cox\'s Bazar', 'কক্স বাজার', 0, 0, 'www.coxsbazar.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(46, 2, 'Feni', 'ফেনী', 23.023231, 91.3840844, 'www.feni.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(47, 2, 'Khagrachari', 'খাগড়াছড়ি', 23.119285, 91.984663, 'www.khagrachhari.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(48, 2, 'Lakshmipur', 'লক্ষ্মীপুর', 22.942477, 90.841184, 'www.lakshmipur.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(49, 2, 'Noakhali', 'নোয়াখালী', 22.869563, 91.099398, 'www.noakhali.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(50, 2, 'Rangamati', 'রাঙ্গামাটি', 0, 0, 'www.rangamati.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(51, 7, 'Habiganj', 'হবিগঞ্জ', 24.374945, 91.41553, 'www.habiganj.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(52, 7, 'Maulvibazar', 'মৌলভীবাজার', 24.482934, 91.777417, 'www.moulvibazar.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(53, 7, 'Sunamganj', 'সুনামগঞ্জ', 25.0658042, 91.3950115, 'www.sunamganj.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(54, 7, 'Sylhet', 'সিলেট', 24.8897956, 91.8697894, 'www.sylhet.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(55, 4, 'Bagerhat', 'বাগেরহাট', 22.651568, 89.785938, 'www.bagerhat.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(56, 4, 'Chuadanga', 'চুয়াডাঙ্গা', 23.6401961, 88.841841, 'www.chuadanga.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(57, 4, 'Jessore', 'যশোর', 23.16643, 89.2081126, 'www.jessore.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(58, 4, 'Jhenaidah', 'ঝিনাইদহ', 23.5448176, 89.1539213, 'www.jhenaidah.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(59, 4, 'Khulna', 'খুলনা', 22.815774, 89.568679, 'www.khulna.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(60, 4, 'Kushtia', 'কুষ্টিয়া', 23.901258, 89.120482, 'www.kushtia.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(61, 4, 'Magura', 'মাগুরা', 23.487337, 89.419956, 'www.magura.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(62, 4, 'Meherpur', 'মেহেরপুর', 23.762213, 88.631821, 'www.meherpur.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(63, 4, 'Narail', 'নড়াইল', 23.172534, 89.512672, 'www.narail.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20'),
(64, 4, 'Satkhira', 'সাতক্ষীরা', 0, 0, 'www.satkhira.gov.bd', '2015-09-13 14:33:27', '2015-09-13 14:36:20');

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` int(2) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `bn_name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `bn_name`, `created_at`, `updated_at`) VALUES
(1, 'Barisal', 'বরিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Chittagong', 'চট্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Dhaka', 'ঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Khulna', 'খুলনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Rajshahi', 'রাজশাহী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Rangpur', 'রংপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Sylhet', 'সিলেট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Mymensingh', 'ময়মনসিংহ', '2016-04-06 20:46:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_hospitals`
--

CREATE TABLE `doctor_hospitals` (
  `id` int(10) UNSIGNED NOT NULL,
  `doctor_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital_id` int(10) UNSIGNED NOT NULL,
  `assigned` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_fees` int(11) DEFAULT NULL,
  `second_fees` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctor_hospitals`
--

INSERT INTO `doctor_hospitals` (`id`, `doctor_id`, `hospital_id`, `assigned`, `first_fees`, `second_fees`, `created_at`, `updated_at`) VALUES
(1, 'd2f9b130-05cd-11e8-b821-45a03bb64b17', 1, 'yes', 800, 500, '2018-01-30 19:57:00', '2018-01-30 19:57:00'),
(2, 'e6e341a0-05ce-11e8-a62c-d91c07185ba0', 1, 'yes', 500, 400, '2018-01-30 20:04:42', '2018-01-30 20:04:42'),
(3, 'a99bffe0-05d0-11e8-93dc-db5269c91fae', 5, 'yes', 500, 400, '2018-01-30 20:17:19', '2018-01-30 20:17:19'),
(4, 'c498afb0-05e3-11e8-8df8-9f4f2251db7e', 4, 'yes', 800, 600, '2018-01-30 22:34:04', '2018-01-30 22:34:04'),
(6, '67dd3c60-05e4-11e8-aa56-ab6bfeb2922a', 6, 'yes', NULL, NULL, '2018-01-30 22:38:38', '2018-01-30 22:38:38'),
(7, '8c12f350-064d-11e8-b818-2553cffac2d6', 6, 'yes', 1000, 600, '2018-01-31 11:11:16', '2018-01-31 11:11:16'),
(8, '9d156fa0-064e-11e8-adbd-bdc6d70d8324', 7, 'yes', 500, 400, '2018-01-31 11:18:54', '2018-01-31 11:18:54');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_profiles`
--

CREATE TABLE `doctor_profiles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `speciality` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `degree` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `awards` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctor_profiles`
--

INSERT INTO `doctor_profiles` (`id`, `name`, `email`, `password`, `speciality`, `degree`, `phone`, `address`, `member`, `awards`, `experience`, `photo`, `remember_token`, `created_at`, `updated_at`) VALUES
('67dd3c60-05e4-11e8-aa56-ab6bfeb2922a', 'Dr. Rafiq', 'rafiq@gmail.com', '$2y$10$CKBEuwTrBwqzU7CZQUmZ4.kxWGy0Dwjk/SCzcn191Uo1YEbL9V4/.', 'Gastroenterology', 'MBBS(Dhaka), FRCS(London), UKLM (USA)', '543654676', 'Dhaka', 'Heart club', 'Safe heart 2017', '15', '533187.png', NULL, '2018-01-30 22:38:38', '2018-01-30 22:38:38'),
('8c12f350-064d-11e8-b818-2553cffac2d6', 'Dr. Mustafiz', 'mustafiz@gmail.com', '$2y$10$fpf6lF0.ASKcqsMJezPJZuqE7pdaPwqWRmLPZjb90guIfprU9usM.', 'Medicine', 'MBBS(Dhaka), FRCS(London), UKLM (USA)', '54365468678', 'Dhaka', 'Heart club', 'Safe heart 2017', '15', '139233.png', NULL, '2018-01-31 11:11:16', '2018-01-31 11:11:16'),
('9d156fa0-064e-11e8-adbd-bdc6d70d8324', 'Dr. Rafiq', 'rafiq43@gmail.com', '$2y$10$ZeIvj5c/DyUsCNhQ74/O8uuBrfge7jWMPJ/EBcJr5E.Kd7Tz/kr2y', 'Orthopaedics', 'MBBS(Dhaka), FRCS(London), UKLM (USA)', '54365467656', 'Dhaka', 'Dental society, BD', 'Best Doctor 2017', '15', '690893.jpg', NULL, '2018-01-31 11:18:54', '2018-01-31 11:18:54'),
('a99bffe0-05d0-11e8-93dc-db5269c91fae', 'Dr. Samsuddin', 'sam@gmail.com', '$2y$10$SoeeVA/fREkMbIRWBEa3YO9KQfKPuyUT0wcsO.piE3jLAr87TAzye', 'Cardiology', 'MBBS(Dhaka), FRCS(London), UKLM (USA)', '5436546323', 'Dhaka', 'Heart club', 'Safe heart 2017', '23', '581621.jpg', 'gLA4o4LOpuXnqptokacgGSs1mmfSqSXq28MguXo8LUglSAuQlR8vuphdQugQ', '2018-01-30 20:17:19', '2018-01-30 20:17:19'),
('c498afb0-05e3-11e8-8df8-9f4f2251db7e', 'Dr. Abdul Jobbar', 'abdul212@gmail.com', '$2y$10$MePZxRGJ/URG5fdIo3acg.NgIThjeKWcQaNKiob1MMbZRGLk0grFS', 'Cardiology', 'MBBS(Dhaka), FRCS(London), UKLM (USA)', '5436546', 'Dhaka', 'Heart club', 'Safe heart 2017', '15', '617235.jpg', NULL, '2018-01-30 22:34:04', '2018-01-30 22:34:04'),
('d2f9b130-05cd-11e8-b821-45a03bb64b17', 'Dr. Azad haque', 'azad@gmail.com', '$2y$10$dI5O6BWkYz6kr.NxvJAMwOuxlmmwLCnfBU21jtG.Y5DFKQ.oiJkhm', 'Cardiology', 'MBBS(Dhaka), FRCS(London)', '5436546321', 'Dhaka', 'Heart club', 'Best Doctor 2017', '14', '616586.jpg', NULL, '2018-01-30 19:57:00', '2018-01-30 19:57:00'),
('e6e341a0-05ce-11e8-a62c-d91c07185ba0', 'Dr. Abul Kalam', 'kalam@gmail.com', '$2y$10$5pHPsQcJ/Q7cxwK0nIyYCuzuE/rRFyEW6JtcXkibgUBIlhp.1.WBO', 'Dental', 'BDS(Dhaka), FRCS(London)', '5436546234', 'Dhaka', 'Dental society, BD', 'Best Doctor 2017', '15', '804369.jpg', 'OYEaAwn1grC05SiENMaNyYeG4sDV8d0XEegJFdyVVSlG4I1HgX2NoAi51lFO', '2018-01-30 20:04:42', '2018-01-30 20:04:42');

-- --------------------------------------------------------

--
-- Table structure for table `donars`
--

CREATE TABLE `donars` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blood_group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upozila` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donars`
--

INSERT INTO `donars` (`id`, `name`, `address`, `phone`, `date`, `blood_group`, `email`, `area`, `upozila`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Md.Saem Hossain', 'kallyanpur', '01744616600', '01/12/17', 'B+', 'saem.cse.diu@gmail.com', 'Dhaka', 'Tongi', 0, '2018-01-30 19:55:32', '2018-01-30 19:55:32'),
(2, 'Sumon', 'Dhanmondi', '01710138034', 'none', 'B+', 'sumon.npi@gmail.com', 'Dhaka', 'Sadarpur', 0, '2018-01-30 20:02:32', '2018-01-30 20:02:32'),
(3, 'Md. Nahid Islam', 'Dhaka, Armanitola', '01874680376', '12/08/2017', 'O+', 'nahid940@gmail.com', 'Dhaka', 'Amtali', 0, '2018-01-30 20:10:25', '2018-01-30 20:10:25'),
(4, 'MD. Sana Ullah Fahim', 'Dhaka Farmgat', '01717852963', '01/03/2017', 'B+', 'fahim@gmial.com', 'Gazipur', 'Kasba', 0, '2018-01-30 20:43:23', '2018-01-30 20:43:23'),
(5, 'Mak Dalim', '1st Lane, Kalabagan, Dhaka', '01715534353', '26/10/2017', 'AB+', 'mak.dalim.jr@gmail.com', 'Comilla', 'Comilla Sadar South', 0, '2018-01-30 21:47:07', '2018-01-30 21:47:07'),
(6, 'Shiblu bhushan mitra', 'Das para, Boro Boyra, Khulna', '01832080201', '01/10/2017', 'O+', 'shiblu940@gmail.com', 'Khulna', 'Others', 0, '2018-01-30 23:38:11', '2018-01-30 23:38:11'),
(7, '.ENG.MD EDRISH AKASH', 'Road No.1, KoLLanpur,Dhaka', '8801830804480', '23/12/2011', 'O+', 'engmdedrish@facebook.com', 'Dhaka', 'Babuganj', 0, '2018-01-31 09:39:30', '2018-01-31 09:39:30');

-- --------------------------------------------------------

--
-- Table structure for table `hospital`
--

CREATE TABLE `hospital` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hospital`
--

INSERT INTO `hospital` (`id`, `name`, `location`, `service`, `about`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Square', '18/F West , Bir Uttam Qazi, Nuruzzaman Road, Panthapath,', 'operation,medicine', 'General hospital in Dhaka, Bangladesh', '848579.png', '2018-01-30 19:55:15', '2018-01-30 19:55:15'),
(3, 'Birdem Hospital', 'shahabagh', 'operation,medicine', 'Bangladesh top private medical', '307292.jpg', '2018-01-30 20:01:37', '2018-01-30 20:01:37'),
(4, 'Popular Hospital', 'science lab,mirpur road,dhanmondi', 'operation,medicine', 'Best hospital in our country', '388639.jpg', '2018-01-30 20:03:35', '2018-01-30 20:03:35'),
(5, 'Apollo Hospital', 'bashundhara,dhaka', 'operation,medicine', 'Best hospital in our country', '411201.jpg', '2018-01-30 20:04:50', '2018-01-30 20:04:50'),
(6, 'Dhaka medical college hospital', 'dhaka', 'operation,medicine', 'Best hospital in our country', '158640.jpg', '2018-01-30 20:07:48', '2018-01-30 20:07:48'),
(7, 'Labaid Hospital', 'mirpur road,Dhanmondi', 'operation,medicine', 'Best hospital in our country', '587750.jpg', '2018-01-30 20:15:21', '2018-01-30 20:15:21'),
(8, 'Ibn Sina Hospital', 'Dhanmondi,dhaka', 'operation,medicine', 'best hospital', '563569.png', '2018-02-01 16:17:54', '2018-02-01 16:17:54');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medical_equipment`
--

CREATE TABLE `medical_equipment` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medicine_supplier`
--

CREATE TABLE `medicine_supplier` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_10_12_000001_create_Degree_table', 1),
(4, '2017_10_12_000002_create_Specialist_table', 1),
(5, '2017_10_12_000003_create_Hospital_table', 1),
(6, '2017_10_12_000004_create_Designation_table', 1),
(7, '2017_10_12_000006_create_Doctor_Profiles_table', 1),
(8, '2017_10_12_000012_create_blogs_table', 1),
(9, '2017_10_12_000013_create_Location_table', 1),
(10, '2017_12_01_193026_create_Medicine_Supplier_table', 1),
(11, '2017_12_01_193141_create_Medical_Equipment_table', 1),
(12, '2017_12_17_043205_create_days_table', 1),
(13, '2017_12_17_043435_create_dcotor_schedules_table', 1),
(14, '2017_12_22_174801_create_donars_table', 1),
(15, '2017_12_28_010953_create_Tests_table', 1),
(16, '2017_12_28_011132_create_Diagnostic_Center_table', 1),
(17, '2017_12_28_011316_create_Diagnostic_Center_Tests_table', 1),
(18, '2017_12_28_131154_create_doctor_hospitals_table', 1),
(19, '2018_01_03_050912_create_seakers_table', 1),
(20, '2018_01_04_085439_create_appointments_table', 1),
(21, '2018_01_08_105006_create_comments_table', 1),
(22, '2018_01_13_060447_create_news_table', 1),
(23, '2018_01_25_164748_create_roles_table', 1),
(24, '2018_01_25_165004_create_admins_table', 1),
(25, '2018_01_25_165210_create_admin_role_table', 1),
(26, '2018_01_28_105643_create__testorder_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_post` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'supportadmin', '2018-01-30 13:56:55', '2018-01-30 13:56:55'),
(2, 'superadmin', '2018-01-30 13:56:55', '2018-01-30 13:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `seakers`
--

CREATE TABLE `seakers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `haddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relationship` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `donor_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seakers`
--

INSERT INTO `seakers` (`id`, `name`, `hname`, `haddress`, `relationship`, `phone`, `email`, `donor_id`, `created_at`, `updated_at`) VALUES
(1, 'Fahim Ahmed', 'Birdem Hospital', 'Shahabug', 'Girlfriend', '01717123123', 'girlfriend@gmial.com', 5, '2018-01-31 09:33:30', '2018-01-31 09:33:30');

-- --------------------------------------------------------

--
-- Table structure for table `specialist`
--

CREATE TABLE `specialist` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `name`, `price`, `created_at`, `updated_at`) VALUES
(1, 'BLOOD GROUP ABO & Rh FACTOR', '350', '2018-01-31 09:59:07', '2018-02-01 11:52:02'),
(2, '3a-Androstanediol Glucuronide, ELISA', '600', '2018-01-31 10:00:35', '2018-01-31 10:00:35'),
(3, 'E-Cadherin, IHC with Interpretation', '400', '2018-01-31 10:01:54', '2018-01-31 10:01:54'),
(4, 'Adenosine Deaminase, Peritoneal Fluid', '2500', '2018-01-31 10:03:04', '2018-01-31 10:03:04');

-- --------------------------------------------------------

--
-- Table structure for table `upazilas`
--

CREATE TABLE `upazilas` (
  `id` int(2) UNSIGNED NOT NULL,
  `district_id` int(2) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `bn_name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upazilas`
--

INSERT INTO `upazilas` (`id`, `district_id`, `name`, `bn_name`, `created_at`, `updated_at`) VALUES
(1, 34, 'Amtali', 'আমতলী', '0000-00-00 00:00:00', '2016-04-06 16:48:39'),
(2, 34, 'Bamna ', 'বামনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 34, 'Barguna Sadar ', 'বরগুনা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 34, 'Betagi ', 'বেতাগি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 34, 'Patharghata ', 'পাথরঘাটা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 34, 'Taltali ', 'তালতলী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 35, 'Muladi ', 'মুলাদি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 35, 'Babuganj ', 'বাবুগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 35, 'Agailjhara ', 'আগাইলঝরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 35, 'Barisal Sadar ', 'বরিশাল সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 35, 'Bakerganj ', 'বাকেরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 35, 'Banaripara ', 'বানাড়িপারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 35, 'Gaurnadi ', 'গৌরনদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 35, 'Hizla ', 'হিজলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 35, 'Mehendiganj ', 'মেহেদিগঞ্জ ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 35, 'Wazirpur ', 'ওয়াজিরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 36, 'Bhola Sadar ', 'ভোলা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 36, 'Burhanuddin ', 'বুরহানউদ্দিন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 36, 'Char Fasson ', 'চর ফ্যাশন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 36, 'Daulatkhan ', 'দৌলতখান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 36, 'Lalmohan ', 'লালমোহন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 36, 'Manpura ', 'মনপুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 36, 'Tazumuddin ', 'তাজুমুদ্দিন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 37, 'Jhalokati Sadar ', 'ঝালকাঠি সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 37, 'Kathalia ', 'কাঁঠালিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 37, 'Nalchity ', 'নালচিতি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 37, 'Rajapur ', 'রাজাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 38, 'Bauphal ', 'বাউফল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 38, 'Dashmina ', 'দশমিনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 38, 'Galachipa ', 'গলাচিপা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 38, 'Kalapara ', 'কালাপারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 38, 'Mirzaganj ', 'মির্জাগঞ্জ ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 38, 'Patuakhali Sadar ', 'পটুয়াখালী সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 38, 'Dumki ', 'ডুমকি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 38, 'Rangabali ', 'রাঙ্গাবালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 39, 'Bhandaria', 'ভ্যান্ডারিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 39, 'Kaukhali', 'কাউখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 39, 'Mathbaria', 'মাঠবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 39, 'Nazirpur', 'নাজিরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 39, 'Nesarabad', 'নেসারাবাদ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 39, 'Pirojpur Sadar', 'পিরোজপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 39, 'Zianagar', 'জিয়ানগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 40, 'Bandarban Sadar', 'বান্দরবন সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 40, 'Thanchi', 'থানচি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 40, 'Lama', 'লামা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 40, 'Naikhongchhari', 'নাইখংছড়ি ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 40, 'Ali kadam', 'আলী কদম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 40, 'Rowangchhari', 'রউয়াংছড়ি ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 40, 'Ruma', 'রুমা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 41, 'Brahmanbaria Sadar ', 'ব্রাহ্মণবাড়িয়া সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 41, 'Ashuganj ', 'আশুগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 41, 'Nasirnagar ', 'নাসির নগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 41, 'Nabinagar ', 'নবীনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 41, 'Sarail ', 'সরাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 41, 'Shahbazpur Town', 'শাহবাজপুর টাউন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 41, 'Kasba ', 'কসবা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 41, 'Akhaura ', 'আখাউরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 41, 'Bancharampur ', 'বাঞ্ছারামপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 41, 'Bijoynagar ', 'বিজয় নগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 42, 'Chandpur Sadar', 'চাঁদপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 42, 'Faridganj', 'ফরিদগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 42, 'Haimchar', 'হাইমচর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 42, 'Haziganj', 'হাজীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 42, 'Kachua', 'কচুয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 42, 'Matlab Uttar', 'মতলব উত্তর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 42, 'Matlab Dakkhin', 'মতলব দক্ষিণ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 42, 'Shahrasti', 'শাহরাস্তি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 43, 'Anwara ', 'আনোয়ারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 43, 'Banshkhali ', 'বাশখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 43, 'Boalkhali ', 'বোয়ালখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 43, 'Chandanaish ', 'চন্দনাইশ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 43, 'Fatikchhari ', 'ফটিকছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 43, 'Hathazari ', 'হাঠহাজারী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 43, 'Lohagara ', 'লোহাগারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 43, 'Mirsharai ', 'মিরসরাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 43, 'Patiya ', 'পটিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 43, 'Rangunia ', 'রাঙ্গুনিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 43, 'Raozan ', 'রাউজান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 43, 'Sandwip ', 'সন্দ্বীপ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 43, 'Satkania ', 'সাতকানিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 43, 'Sitakunda ', 'সীতাকুণ্ড', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 44, 'Barura ', 'বড়ুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 44, 'Brahmanpara ', 'ব্রাহ্মণপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 44, 'Burichong ', 'বুড়িচং', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 44, 'Chandina ', 'চান্দিনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 44, 'Chauddagram ', 'চৌদ্দগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 44, 'Daudkandi ', 'দাউদকান্দি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 44, 'Debidwar ', 'দেবীদ্বার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 44, 'Homna ', 'হোমনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 44, 'Comilla Sadar ', 'কুমিল্লা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 44, 'Laksam ', 'লাকসাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 44, 'Monohorgonj ', 'মনোহরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 44, 'Meghna ', 'মেঘনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 44, 'Muradnagar ', 'মুরাদনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 44, 'Nangalkot ', 'নাঙ্গালকোট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 44, 'Comilla Sadar South ', 'কুমিল্লা সদর দক্ষিণ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 44, 'Titas ', 'তিতাস', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 45, 'Chakaria ', 'চকরিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 45, 'Chakaria ', 'চকরিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 45, 'Cox\'s Bazar Sadar ', 'কক্স বাজার সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 45, 'Kutubdia ', 'কুতুবদিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 45, 'Maheshkhali ', 'মহেশখালী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 45, 'Ramu ', 'রামু', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 45, 'Teknaf ', 'টেকনাফ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 45, 'Ukhia ', 'উখিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 45, 'Pekua ', 'পেকুয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 46, 'Feni Sadar', 'ফেনী সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 46, 'Chagalnaiya', 'ছাগল নাইয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 46, 'Daganbhyan', 'দাগানভিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 46, 'Parshuram', 'পরশুরাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 46, 'Fhulgazi', 'ফুলগাজি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 46, 'Sonagazi', 'সোনাগাজি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 47, 'Dighinala ', 'দিঘিনালা ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 47, 'Khagrachhari ', 'খাগড়াছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 47, 'Lakshmichhari ', 'লক্ষ্মীছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 47, 'Mahalchhari ', 'মহলছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 47, 'Manikchhari ', 'মানিকছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 47, 'Matiranga ', 'মাটিরাঙ্গা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 47, 'Panchhari ', 'পানছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 47, 'Ramgarh ', 'রামগড়', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 48, 'Lakshmipur Sadar ', 'লক্ষ্মীপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 48, 'Raipur ', 'রায়পুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 48, 'Ramganj ', 'রামগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 48, 'Ramgati ', 'রামগতি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 48, 'Komol Nagar ', 'কমল নগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 49, 'Noakhali Sadar ', 'নোয়াখালী সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 49, 'Begumganj ', 'বেগমগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 49, 'Chatkhil ', 'চাটখিল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 49, 'Companyganj ', 'কোম্পানীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 49, 'Shenbag ', 'শেনবাগ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 49, 'Hatia ', 'হাতিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 49, 'Kobirhat ', 'কবিরহাট ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 49, 'Sonaimuri ', 'সোনাইমুরি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 49, 'Suborno Char ', 'সুবর্ণ চর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 50, 'Rangamati Sadar ', 'রাঙ্গামাটি সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 50, 'Belaichhari ', 'বেলাইছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 50, 'Bagaichhari ', 'বাঘাইছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 50, 'Barkal ', 'বরকল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 50, 'Juraichhari ', 'জুরাইছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 50, 'Rajasthali ', 'রাজাস্থলি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 50, 'Kaptai ', 'কাপ্তাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 50, 'Langadu ', 'লাঙ্গাডু', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 50, 'Nannerchar ', 'নান্নেরচর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 50, 'Kaukhali ', 'কাউখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 2, 'Faridpur Sadar ', 'ফরিদপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 2, 'Boalmari ', 'বোয়ালমারী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 2, 'Alfadanga ', 'আলফাডাঙ্গা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 2, 'Madhukhali ', 'মধুখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 2, 'Bhanga ', 'ভাঙ্গা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 2, 'Nagarkanda ', 'নগরকান্ড', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 2, 'Charbhadrasan ', 'চরভদ্রাসন ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 2, 'Sadarpur ', 'সদরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 2, 'Shaltha ', 'শালথা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 3, 'Gazipur Sadar-Joydebpur', 'গাজীপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 3, 'Kaliakior', 'কালিয়াকৈর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 3, 'Kapasia', 'কাপাসিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 3, 'Sripur', 'শ্রীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 3, 'Kaliganj', 'কালীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 3, 'Tongi', 'টঙ্গি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 4, 'Gopalganj Sadar ', 'গোপালগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, 4, 'Kashiani ', 'কাশিয়ানি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, 4, 'Kotalipara ', 'কোটালিপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, 4, 'Muksudpur ', 'মুকসুদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, 4, 'Tungipara ', 'টুঙ্গিপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, 5, 'Dewanganj ', 'দেওয়ানগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, 5, 'Baksiganj ', 'বকসিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, 5, 'Islampur ', 'ইসলামপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, 5, 'Jamalpur Sadar ', 'জামালপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, 5, 'Madarganj ', 'মাদারগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, 5, 'Melandaha ', 'মেলানদাহা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, 5, 'Sarishabari ', 'সরিষাবাড়ি ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, 5, 'Narundi Police I.C', 'নারুন্দি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, 6, 'Astagram ', 'অষ্টগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, 6, 'Bajitpur ', 'বাজিতপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, 6, 'Bhairab ', 'ভৈরব', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, 6, 'Hossainpur ', 'হোসেনপুর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, 6, 'Itna ', 'ইটনা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, 6, 'Karimganj ', 'করিমগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, 6, 'Katiadi ', 'কতিয়াদি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, 6, 'Kishoreganj Sadar ', 'কিশোরগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, 6, 'Kuliarchar ', 'কুলিয়ারচর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, 6, 'Mithamain ', 'মিঠামাইন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, 6, 'Nikli ', 'নিকলি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, 6, 'Pakundia ', 'পাকুন্ডা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, 6, 'Tarail ', 'তাড়াইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, 7, 'Madaripur Sadar', 'মাদারীপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, 7, 'Kalkini', 'কালকিনি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, 7, 'Rajoir', 'রাজইর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, 7, 'Shibchar', 'শিবচর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, 8, 'Manikganj Sadar ', 'মানিকগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, 8, 'Singair ', 'সিঙ্গাইর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, 8, 'Shibalaya ', 'শিবালয়', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, 8, 'Saturia ', 'সাঠুরিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, 8, 'Harirampur ', 'হরিরামপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, 8, 'Ghior ', 'ঘিওর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, 8, 'Daulatpur ', 'দৌলতপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, 9, 'Lohajang ', 'লোহাজং', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, 9, 'Sreenagar ', 'শ্রীনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, 9, 'Munshiganj Sadar ', 'মুন্সিগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, 9, 'Sirajdikhan ', 'সিরাজদিখান', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, 9, 'Tongibari ', 'টঙ্গিবাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, 9, 'Gazaria ', 'গজারিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, 10, 'Bhaluka', 'ভালুকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, 10, 'Trishal', 'ত্রিশাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(210, 10, 'Haluaghat', 'হালুয়াঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(211, 10, 'Muktagachha', 'মুক্তাগাছা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, 10, 'Dhobaura', 'ধবারুয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, 10, 'Fulbaria', 'ফুলবাড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, 10, 'Gaffargaon', 'গফরগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(215, 10, 'Gauripur', 'গৌরিপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(216, 10, 'Ishwarganj', 'ঈশ্বরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(217, 10, 'Mymensingh Sadar', 'ময়মনসিং সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(218, 10, 'Nandail', 'নন্দাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(219, 10, 'Phulpur', 'ফুলপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(220, 11, 'Araihazar ', 'আড়াইহাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(221, 11, 'Sonargaon ', 'সোনারগাঁও', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(222, 11, 'Bandar', 'বান্দার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(223, 11, 'Naryanganj Sadar ', 'নারায়ানগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(224, 11, 'Rupganj ', 'রূপগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(225, 11, 'Siddirgonj ', 'সিদ্ধিরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(226, 12, 'Belabo ', 'বেলাবো', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, 12, 'Monohardi ', 'মনোহরদি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(228, 12, 'Narsingdi Sadar ', 'নরসিংদী সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, 12, 'Palash ', 'পলাশ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, 12, 'Raipura , Narsingdi', 'রায়পুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, 12, 'Shibpur ', 'শিবপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, 13, 'Kendua Upazilla', 'কেন্দুয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(233, 13, 'Atpara Upazilla', 'আটপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(234, 13, 'Barhatta Upazilla', 'বরহাট্টা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(235, 13, 'Durgapur Upazilla', 'দুর্গাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, 13, 'Kalmakanda Upazilla', 'কলমাকান্দা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, 13, 'Madan Upazilla', 'মদন', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(238, 13, 'Mohanganj Upazilla', 'মোহনগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(239, 13, 'Netrakona-S Upazilla', 'নেত্রকোনা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(240, 13, 'Purbadhala Upazilla', 'পূর্বধলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(241, 13, 'Khaliajuri Upazilla', 'খালিয়াজুরি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, 14, 'Baliakandi ', 'বালিয়াকান্দি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, 14, 'Goalandaghat ', 'গোয়ালন্দ ঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, 14, 'Pangsha ', 'পাংশা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, 14, 'Kalukhali ', 'কালুখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, 14, 'Rajbari Sadar ', 'রাজবাড়ি সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, 15, 'Shariatpur Sadar -Palong', 'শরীয়তপুর সদর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, 15, 'Damudya ', 'দামুদিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, 15, 'Naria ', 'নড়িয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, 15, 'Jajira ', 'জাজিরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, 15, 'Bhedarganj ', 'ভেদারগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, 15, 'Gosairhat ', 'গোসাইর হাট ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, 16, 'Jhenaigati ', 'ঝিনাইগাতি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, 16, 'Nakla ', 'নাকলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, 16, 'Nalitabari ', 'নালিতাবাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, 16, 'Sherpur Sadar ', 'শেরপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, 16, 'Sreebardi ', 'শ্রীবরদি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, 17, 'Tangail Sadar ', 'টাঙ্গাইল সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, 17, 'Sakhipur ', 'সখিপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, 17, 'Basail ', 'বসাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, 17, 'Madhupur ', 'মধুপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, 17, 'Ghatail ', 'ঘাটাইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, 17, 'Kalihati ', 'কালিহাতি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, 17, 'Nagarpur ', 'নগরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, 17, 'Mirzapur ', 'মির্জাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, 17, 'Gopalpur ', 'গোপালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, 17, 'Delduar ', 'দেলদুয়ার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, 17, 'Bhuapur ', 'ভুয়াপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, 17, 'Dhanbari ', 'ধানবাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, 55, 'Bagerhat Sadar ', 'বাগেরহাট সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, 55, 'Chitalmari ', 'চিতলমাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, 55, 'Fakirhat ', 'ফকিরহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, 55, 'Kachua ', 'কচুয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, 55, 'Mollahat ', 'মোল্লাহাট ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, 55, 'Mongla ', 'মংলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, 55, 'Morrelganj ', 'মরেলগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, 55, 'Rampal ', 'রামপাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, 55, 'Sarankhola ', 'স্মরণখোলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, 56, 'Damurhuda ', 'দামুরহুদা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, 56, 'Chuadanga-S ', 'চুয়াডাঙ্গা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, 56, 'Jibannagar ', 'জীবন নগর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, 56, 'Alamdanga ', 'আলমডাঙ্গা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, 57, 'Abhaynagar ', 'অভয়নগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, 57, 'Keshabpur ', 'কেশবপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(285, 57, 'Bagherpara ', 'বাঘের পাড়া ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, 57, 'Jessore Sadar ', 'যশোর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, 57, 'Chaugachha ', 'চৌগাছা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, 57, 'Manirampur ', 'মনিরামপুর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, 57, 'Jhikargachha ', 'ঝিকরগাছা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, 57, 'Sharsha ', 'সারশা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, 58, 'Jhenaidah Sadar ', 'ঝিনাইদহ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(292, 58, 'Maheshpur ', 'মহেশপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(293, 58, 'Kaliganj ', 'কালীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(294, 58, 'Kotchandpur ', 'কোট চাঁদপুর ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(295, 58, 'Shailkupa ', 'শৈলকুপা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(296, 58, 'Harinakunda ', 'হাড়িনাকুন্দা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(297, 59, 'Terokhada ', 'তেরোখাদা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(298, 59, 'Batiaghata ', 'বাটিয়াঘাটা ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(299, 59, 'Dacope ', 'ডাকপে', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(300, 59, 'Dumuria ', 'ডুমুরিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(301, 59, 'Dighalia ', 'দিঘলিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(302, 59, 'Koyra ', 'কয়ড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(303, 59, 'Paikgachha ', 'পাইকগাছা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(304, 59, 'Phultala ', 'ফুলতলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(305, 59, 'Rupsa ', 'রূপসা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(306, 60, 'Kushtia Sadar', 'কুষ্টিয়া সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(307, 60, 'Kumarkhali', 'কুমারখালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(308, 60, 'Daulatpur', 'দৌলতপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(309, 60, 'Mirpur', 'মিরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(310, 60, 'Bheramara', 'ভেরামারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(311, 60, 'Khoksa', 'খোকসা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(312, 61, 'Magura Sadar ', 'মাগুরা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(313, 61, 'Mohammadpur ', 'মোহাম্মাদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(314, 61, 'Shalikha ', 'শালিখা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(315, 61, 'Sreepur ', 'শ্রীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(316, 62, 'angni ', 'আংনি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(317, 62, 'Mujib Nagar ', 'মুজিব নগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(318, 62, 'Meherpur-S ', 'মেহেরপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(319, 63, 'Narail-S Upazilla', 'নড়াইল সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(320, 63, 'Lohagara Upazilla', 'লোহাগাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(321, 63, 'Kalia Upazilla', 'কালিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(322, 64, 'Satkhira Sadar ', 'সাতক্ষীরা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(323, 64, 'Assasuni ', 'আসসাশুনি ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(324, 64, 'Debhata ', 'দেভাটা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(325, 64, 'Tala ', 'তালা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(326, 64, 'Kalaroa ', 'কলরোয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(327, 64, 'Kaliganj ', 'কালীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(328, 64, 'Shyamnagar ', 'শ্যামনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(329, 18, 'Adamdighi', 'আদমদিঘী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(330, 18, 'Bogra Sadar', 'বগুড়া সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(331, 18, 'Sherpur', 'শেরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(332, 18, 'Dhunat', 'ধুনট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(333, 18, 'Dhupchanchia', 'দুপচাচিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(334, 18, 'Gabtali', 'গাবতলি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(335, 18, 'Kahaloo', 'কাহালু', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(336, 18, 'Nandigram', 'নন্দিগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(337, 18, 'Sahajanpur', 'শাহজাহানপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(338, 18, 'Sariakandi', 'সারিয়াকান্দি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(339, 18, 'Shibganj', 'শিবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(340, 18, 'Sonatala', 'সোনাতলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(341, 19, 'Joypurhat S', 'জয়পুরহাট সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(342, 19, 'Akkelpur', 'আক্কেলপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(343, 19, 'Kalai', 'কালাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(344, 19, 'Khetlal', 'খেতলাল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(345, 19, 'Panchbibi', 'পাঁচবিবি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(346, 20, 'Naogaon Sadar ', 'নওগাঁ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(347, 20, 'Mohadevpur ', 'মহাদেবপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(348, 20, 'Manda ', 'মান্দা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(349, 20, 'Niamatpur ', 'নিয়ামতপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(350, 20, 'Atrai ', 'আত্রাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(351, 20, 'Raninagar ', 'রাণীনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(352, 20, 'Patnitala ', 'পত্নীতলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(353, 20, 'Dhamoirhat ', 'ধামইরহাট ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(354, 20, 'Sapahar ', 'সাপাহার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(355, 20, 'Porsha ', 'পোরশা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(356, 20, 'Badalgachhi ', 'বদলগাছি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(357, 21, 'Natore Sadar ', 'নাটোর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(358, 21, 'Baraigram ', 'বড়াইগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(359, 21, 'Bagatipara ', 'বাগাতিপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(360, 21, 'Lalpur ', 'লালপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(361, 21, 'Natore Sadar ', 'নাটোর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(362, 21, 'Baraigram ', 'বড়াই গ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(363, 22, 'Bholahat ', 'ভোলাহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(364, 22, 'Gomastapur ', 'গোমস্তাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(365, 22, 'Nachole ', 'নাচোল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(366, 22, 'Nawabganj Sadar ', 'নবাবগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(367, 22, 'Shibganj ', 'শিবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(368, 23, 'Atgharia ', 'আটঘরিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(369, 23, 'Bera ', 'বেড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(370, 23, 'Bhangura ', 'ভাঙ্গুরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(371, 23, 'Chatmohar ', 'চাটমোহর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(372, 23, 'Faridpur ', 'ফরিদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(373, 23, 'Ishwardi ', 'ঈশ্বরদী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(374, 23, 'Pabna Sadar ', 'পাবনা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(375, 23, 'Santhia ', 'সাথিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(376, 23, 'Sujanagar ', 'সুজানগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(377, 24, 'Bagha', 'বাঘা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(378, 24, 'Bagmara', 'বাগমারা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(379, 24, 'Charghat', 'চারঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(380, 24, 'Durgapur', 'দুর্গাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(381, 24, 'Godagari', 'গোদাগারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(382, 24, 'Mohanpur', 'মোহনপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(383, 24, 'Paba', 'পবা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(384, 24, 'Puthia', 'পুঠিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(385, 24, 'Tanore', 'তানোর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(386, 25, 'Sirajganj Sadar ', 'সিরাজগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(387, 25, 'Belkuchi ', 'বেলকুচি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(388, 25, 'Chauhali ', 'চৌহালি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(389, 25, 'Kamarkhanda ', 'কামারখান্দা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(390, 25, 'Kazipur ', 'কাজীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(391, 25, 'Raiganj ', 'রায়গঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(392, 25, 'Shahjadpur ', 'শাহজাদপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(393, 25, 'Tarash ', 'তারাশ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(394, 25, 'Ullahpara ', 'উল্লাপাড়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(395, 26, 'Birampur ', 'বিরামপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(396, 26, 'Birganj', 'বীরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(397, 26, 'Biral ', 'বিড়াল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(398, 26, 'Bochaganj ', 'বোচাগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(399, 26, 'Chirirbandar ', 'চিরিরবন্দর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(400, 26, 'Phulbari ', 'ফুলবাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(401, 26, 'Ghoraghat ', 'ঘোড়াঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(402, 26, 'Hakimpur ', 'হাকিমপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(403, 26, 'Kaharole ', 'কাহারোল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(404, 26, 'Khansama ', 'খানসামা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(405, 26, 'Dinajpur Sadar ', 'দিনাজপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(406, 26, 'Nawabganj', 'নবাবগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(407, 26, 'Parbatipur ', 'পার্বতীপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(408, 27, 'Fulchhari', 'ফুলছড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(409, 27, 'Gaibandha sadar', 'গাইবান্ধা সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(410, 27, 'Gobindaganj', 'গোবিন্দগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(411, 27, 'Palashbari', 'পলাশবাড়ী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(412, 27, 'Sadullapur', 'সাদুল্যাপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(413, 27, 'Saghata', 'সাঘাটা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(414, 27, 'Sundarganj', 'সুন্দরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(415, 28, 'Kurigram Sadar', 'কুড়িগ্রাম সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(416, 28, 'Nageshwari', 'নাগেশ্বরী', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(417, 28, 'Bhurungamari', 'ভুরুঙ্গামারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(418, 28, 'Phulbari', 'ফুলবাড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(419, 28, 'Rajarhat', 'রাজারহাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(420, 28, 'Ulipur', 'উলিপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(421, 28, 'Chilmari', 'চিলমারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(422, 28, 'Rowmari', 'রউমারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(423, 28, 'Char Rajibpur', 'চর রাজিবপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(424, 29, 'Lalmanirhat Sadar', 'লালমনিরহাট সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(425, 29, 'Aditmari', 'আদিতমারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(426, 29, 'Kaliganj', 'কালীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(427, 29, 'Hatibandha', 'হাতিবান্ধা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(428, 29, 'Patgram', 'পাটগ্রাম', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(429, 30, 'Nilphamari Sadar', 'নীলফামারী সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(430, 30, 'Saidpur', 'সৈয়দপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(431, 30, 'Jaldhaka', 'জলঢাকা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(432, 30, 'Kishoreganj', 'কিশোরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(433, 30, 'Domar', 'ডোমার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(434, 30, 'Dimla', 'ডিমলা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(435, 31, 'Panchagarh Sadar', 'পঞ্চগড় সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(436, 31, 'Debiganj', 'দেবীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(437, 31, 'Boda', 'বোদা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(438, 31, 'Atwari', 'আটোয়ারি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(439, 31, 'Tetulia', 'তেতুলিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(440, 32, 'Badarganj', 'বদরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(441, 32, 'Mithapukur', 'মিঠাপুকুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(442, 32, 'Gangachara', 'গঙ্গাচরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(443, 32, 'Kaunia', 'কাউনিয়া', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(444, 32, 'Rangpur Sadar', 'রংপুর সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(445, 32, 'Pirgachha', 'পীরগাছা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(446, 32, 'Pirganj', 'পীরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(447, 32, 'Taraganj', 'তারাগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(448, 33, 'Thakurgaon Sadar ', 'ঠাকুরগাঁও সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(449, 33, 'Pirganj ', 'পীরগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(450, 33, 'Baliadangi ', 'বালিয়াডাঙ্গি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(451, 33, 'Haripur ', 'হরিপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(452, 33, 'Ranisankail ', 'রাণীসংকইল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(453, 51, 'Ajmiriganj', 'আজমিরিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(454, 51, 'Baniachang', 'বানিয়াচং', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(455, 51, 'Bahubal', 'বাহুবল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(456, 51, 'Chunarughat', 'চুনারুঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(457, 51, 'Habiganj Sadar', 'হবিগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(458, 51, 'Lakhai', 'লাক্ষাই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(459, 51, 'Madhabpur', 'মাধবপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(460, 51, 'Nabiganj', 'নবীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(461, 51, 'Shaistagonj ', 'শায়েস্তাগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(462, 52, 'Moulvibazar Sadar', 'মৌলভীবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(463, 52, 'Barlekha', 'বড়লেখা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(464, 52, 'Juri', 'জুড়ি', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(465, 52, 'Kamalganj', 'কামালগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(466, 52, 'Kulaura', 'কুলাউরা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(467, 52, 'Rajnagar', 'রাজনগর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(468, 52, 'Sreemangal', 'শ্রীমঙ্গল', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(469, 53, 'Bishwamvarpur', 'বিসশম্ভারপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(470, 53, 'Chhatak', 'ছাতক', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(471, 53, 'Derai', 'দেড়াই', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(472, 53, 'Dharampasha', 'ধরমপাশা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(473, 53, 'Dowarabazar', 'দোয়ারাবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(474, 53, 'Jagannathpur', 'জগন্নাথপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(475, 53, 'Jamalganj', 'জামালগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(476, 53, 'Sulla', 'সুল্লা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(477, 53, 'Sunamganj Sadar', 'সুনামগঞ্জ সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(478, 53, 'Shanthiganj', 'শান্তিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(479, 53, 'Tahirpur', 'তাহিরপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(480, 54, 'Sylhet Sadar', 'সিলেট সদর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(481, 54, 'Beanibazar', 'বেয়ানিবাজার', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(482, 54, 'Bishwanath', 'বিশ্বনাথ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(483, 54, 'Dakshin Surma ', 'দক্ষিণ সুরমা', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(484, 54, 'Balaganj', 'বালাগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(485, 54, 'Companiganj', 'কোম্পানিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(486, 54, 'Fenchuganj', 'ফেঞ্চুগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(487, 54, 'Golapganj', 'গোলাপগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(488, 54, 'Gowainghat', 'গোয়াইনঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(489, 54, 'Jaintiapur', 'জয়ন্তপুর', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(490, 54, 'Kanaighat', 'কানাইঘাট', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(491, 54, 'Zakiganj', 'জাকিগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(492, 54, 'Nobigonj', 'নবীগঞ্জ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(493, 1, 'Adabor', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(494, 1, 'Airport', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(495, 1, 'Badda', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(496, 1, 'Banani', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(497, 1, 'Bangshal', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(498, 1, 'Bhashantek', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(499, 1, 'Cantonment', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(500, 1, 'Chackbazar', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(501, 1, 'Darussalam', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(502, 1, 'Daskhinkhan', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(503, 1, 'Demra', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(504, 1, 'Dhamrai', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(505, 1, 'Dhanmondi', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(506, 1, 'Dohar', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(507, 1, 'Gandaria', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(508, 1, 'Gulshan', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(509, 1, 'Hazaribag', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(510, 1, 'Jatrabari', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(511, 1, 'Kafrul', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(512, 1, 'Kalabagan', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(513, 1, 'Kamrangirchar', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(514, 1, 'Keraniganj', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(515, 1, 'Khilgaon', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(516, 1, 'Khilkhet', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(517, 1, 'Kotwali', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(518, 1, 'Lalbag', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(519, 1, 'Mirpur Model', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(520, 1, 'Mohammadpur', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(521, 1, 'Motijheel', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(522, 1, 'Mugda', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(523, 1, 'Nawabganj', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(524, 1, 'New Market', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(525, 1, 'Pallabi', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(526, 1, 'Paltan', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(527, 1, 'Ramna', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(528, 1, 'Rampura', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(529, 1, 'Rupnagar', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(530, 1, 'Sabujbag', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(531, 1, 'Savar', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(532, 1, 'Shah Ali', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(533, 1, 'Shahbag', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(534, 1, 'Shahjahanpur', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(535, 1, 'Sherebanglanagar', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(536, 1, 'Shyampur', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(537, 1, 'Sutrapur', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(538, 1, 'Tejgaon', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(539, 1, 'Tejgaon I/A', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(540, 1, 'Turag', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(541, 1, 'Uttara', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(542, 1, 'Uttara West', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(543, 1, 'Uttarkhan', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(544, 1, 'Vatara', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(545, 1, 'Wari', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(546, 1, 'Others', NULL, '2016-04-06 21:00:33', '0000-00-00 00:00:00'),
(547, 35, 'Airport', 'এয়ারপোর্ট', '2016-04-06 21:23:08', '0000-00-00 00:00:00'),
(548, 35, 'Kawnia', 'কাউনিয়া', '2016-04-06 21:24:40', '0000-00-00 00:00:00'),
(549, 35, 'Bondor', 'বন্দর', '2016-04-06 21:27:19', '0000-00-00 00:00:00'),
(550, 35, 'Others', 'অন্যান্য', '2016-04-06 21:28:14', '0000-00-00 00:00:00'),
(551, 24, 'Boalia', 'বোয়ালিয়া', '2016-04-06 21:32:13', '0000-00-00 00:00:00'),
(552, 24, 'Motihar', 'মতিহার', '2016-04-06 21:33:00', '0000-00-00 00:00:00'),
(553, 24, 'Shahmokhdum', 'শাহ্ মকখদুম ', '2016-04-06 21:36:15', '0000-00-00 00:00:00'),
(554, 24, 'Rajpara', 'রাজপারা ', '2016-04-06 21:38:32', '0000-00-00 00:00:00'),
(555, 24, 'Others', 'অন্যান্য', '2016-04-06 21:39:09', '0000-00-00 00:00:00'),
(556, 43, 'Akborsha', 'Akborsha', '2016-04-06 21:57:01', '0000-00-00 00:00:00'),
(557, 43, 'Baijid bostami', 'বাইজিদ বোস্তামী', '2016-04-06 22:09:38', '0000-00-00 00:00:00'),
(558, 43, 'Bakolia', 'বাকোলিয়া', '2016-04-06 22:10:52', '0000-00-00 00:00:00'),
(559, 43, 'Bandar', 'বন্দর', '2016-04-06 22:11:53', '0000-00-00 00:00:00'),
(560, 43, 'Chandgaon', 'চাঁদগাও', '2016-04-06 22:12:34', '0000-00-00 00:00:00'),
(561, 43, 'Chokbazar', 'চকবাজার', '2016-04-06 22:13:10', '0000-00-00 00:00:00'),
(562, 43, 'Doublemooring', 'ডাবল মুরিং', '2016-04-06 22:14:10', '0000-00-00 00:00:00'),
(563, 43, 'EPZ', 'ইপিজেড', '2016-04-06 22:14:55', '0000-00-00 00:00:00'),
(564, 43, 'Hali Shohor', 'হলী শহর', '2016-04-06 22:15:54', '0000-00-00 00:00:00'),
(565, 43, 'Kornafuli', 'কর্ণফুলি', '2016-04-06 22:16:29', '0000-00-00 00:00:00'),
(566, 43, 'Kotwali', 'কোতোয়ালী', '2016-04-06 22:17:08', '0000-00-00 00:00:00'),
(567, 43, 'Kulshi', 'কুলশি', '2016-04-06 22:18:09', '0000-00-00 00:00:00'),
(568, 43, 'Pahartali', 'পাহাড়তলী', '2016-04-06 22:19:26', '0000-00-00 00:00:00'),
(569, 43, 'Panchlaish', 'পাঁচলাইশ', '2016-04-06 22:20:24', '0000-00-00 00:00:00'),
(570, 43, 'Potenga', 'পতেঙ্গা', '2016-04-06 22:21:20', '0000-00-00 00:00:00'),
(571, 43, 'Shodhorgat', 'সদরঘাট', '2016-04-06 22:22:19', '0000-00-00 00:00:00'),
(572, 43, 'Others', 'অন্যান্য', '2016-04-06 22:22:51', '0000-00-00 00:00:00'),
(573, 44, 'Others', 'অন্যান্য', '2016-04-06 22:37:59', '0000-00-00 00:00:00'),
(574, 59, 'Aranghata', 'আড়াংঘাটা', '2016-04-06 23:30:50', '0000-00-00 00:00:00'),
(575, 59, 'Daulatpur', 'দৌলতপুর', '2016-04-06 23:32:12', '0000-00-00 00:00:00'),
(576, 59, 'Harintana', 'হারিন্তানা ', '2016-04-06 23:34:06', '0000-00-00 00:00:00'),
(577, 59, 'Horintana', 'হরিণতানা ', '2016-04-06 23:35:11', '0000-00-00 00:00:00'),
(578, 59, 'Khalishpur', 'খালিশপুর', '2016-04-06 23:35:56', '0000-00-00 00:00:00'),
(579, 59, 'Khanjahan Ali', 'খানজাহান আলী', '2016-04-06 23:37:14', '0000-00-00 00:00:00'),
(580, 59, 'Khulna Sadar', 'খুলনা সদর', '2016-04-06 23:37:58', '0000-00-00 00:00:00'),
(581, 59, 'Labanchora', 'লাবানছোরা', '2016-04-06 23:39:23', '0000-00-00 00:00:00'),
(582, 59, 'Sonadanga', 'সোনাডাঙ্গা', '2016-04-06 23:40:22', '0000-00-00 00:00:00'),
(583, 59, 'Others', 'অন্যান্য', '2016-04-06 23:42:14', '0000-00-00 00:00:00'),
(584, 2, 'Others', 'অন্যান্য', '2016-04-06 23:43:56', '0000-00-00 00:00:00'),
(585, 4, 'Others', 'অন্যান্য', '2016-04-06 23:45:07', '0000-00-00 00:00:00'),
(586, 5, 'Others', 'অন্যান্য', '2016-04-06 23:46:18', '0000-00-00 00:00:00'),
(587, 54, 'Airport', 'বিমানবন্দর', '2016-04-06 23:54:47', '0000-00-00 00:00:00'),
(588, 54, 'Hazrat Shah Paran', 'হযরত শাহ পরাণ', '2016-04-06 23:57:13', '0000-00-00 00:00:00'),
(589, 54, 'Jalalabad', 'জালালাবাদ', '2016-04-06 23:58:15', '0000-00-00 00:00:00'),
(590, 54, 'Kowtali', 'কোতোয়ালী', '2016-04-06 23:59:27', '0000-00-00 00:00:00'),
(591, 54, 'Moglabazar', 'মোগলাবাজার', '2016-04-07 00:00:58', '0000-00-00 00:00:00'),
(592, 54, 'Osmani Nagar', 'ওসমানী নগর', '2016-04-07 00:01:36', '0000-00-00 00:00:00'),
(593, 54, 'South Surma', 'দক্ষিণ সুরমা', '2016-04-07 00:02:16', '0000-00-00 00:00:00'),
(594, 54, 'Others', 'অন্যান্য', '2016-04-07 00:03:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `_testorder`
--

CREATE TABLE `_testorder` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tests_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `_testorder`
--

INSERT INTO `_testorder` (`id`, `name`, `phone`, `address`, `Tests_id`, `created_at`, `updated_at`) VALUES
(1, 'Fahim ahmed', '01717852963', 'Dhaka', 1, '2018-01-31 10:09:06', '2018-01-31 10:09:06'),
(2, 'Fahim ahmed', '01717852963', 'Dhaka', 2, '2018-01-31 10:09:06', '2018-01-31 10:09:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_role_admin_id_foreign` (`admin_id`),
  ADD KEY `admin_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appointments_hospital_id_foreign` (`hospital_id`),
  ADD KEY `appointments_doctor_id_foreign` (`doctor_id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_doctor_id_foreign` (`doctor_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_blog_id_foreign` (`blog_id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dcotor_schedules`
--
ALTER TABLE `dcotor_schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dcotor_schedules_doctor_id_foreign` (`doctor_id`),
  ADD KEY `dcotor_schedules_hospital_id_foreign` (`hospital_id`),
  ADD KEY `dcotor_schedules_day_id_foreign` (`day_id`);

--
-- Indexes for table `degree`
--
ALTER TABLE `degree`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diagnostic_center`
--
ALTER TABLE `diagnostic_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diagnostic_center_tests`
--
ALTER TABLE `diagnostic_center_tests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diagnostic_center_tests_tests_id_foreign` (`Tests_id`),
  ADD KEY `diagnostic_center_tests_diagnostic_center_id_foreign` (`Diagnostic_Center_id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_hospitals`
--
ALTER TABLE `doctor_hospitals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doctor_hospitals_doctor_id_foreign` (`doctor_id`),
  ADD KEY `doctor_hospitals_hospital_id_foreign` (`hospital_id`);

--
-- Indexes for table `doctor_profiles`
--
ALTER TABLE `doctor_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `doctor_profiles_email_unique` (`email`);

--
-- Indexes for table `donars`
--
ALTER TABLE `donars`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `donars_email_unique` (`email`);

--
-- Indexes for table `hospital`
--
ALTER TABLE `hospital`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medical_equipment`
--
ALTER TABLE `medical_equipment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medicine_supplier`
--
ALTER TABLE `medicine_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seakers`
--
ALTER TABLE `seakers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seakers_donor_id_foreign` (`donor_id`);

--
-- Indexes for table `specialist`
--
ALTER TABLE `specialist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upazilas`
--
ALTER TABLE `upazilas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `_testorder`
--
ALTER TABLE `_testorder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `_testorder_tests_id_foreign` (`Tests_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dcotor_schedules`
--
ALTER TABLE `dcotor_schedules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `degree`
--
ALTER TABLE `degree`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `diagnostic_center`
--
ALTER TABLE `diagnostic_center`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `diagnostic_center_tests`
--
ALTER TABLE `diagnostic_center_tests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `doctor_hospitals`
--
ALTER TABLE `doctor_hospitals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `donars`
--
ALTER TABLE `donars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `hospital`
--
ALTER TABLE `hospital`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `medical_equipment`
--
ALTER TABLE `medical_equipment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `medicine_supplier`
--
ALTER TABLE `medicine_supplier`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `seakers`
--
ALTER TABLE `seakers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `specialist`
--
ALTER TABLE `specialist`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `upazilas`
--
ALTER TABLE `upazilas`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=595;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_testorder`
--
ALTER TABLE `_testorder`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD CONSTRAINT `admin_role_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `admin_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `appointments_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `doctor_profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `appointments_hospital_id_foreign` FOREIGN KEY (`hospital_id`) REFERENCES `hospital` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `doctor_profiles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`);

--
-- Constraints for table `dcotor_schedules`
--
ALTER TABLE `dcotor_schedules`
  ADD CONSTRAINT `dcotor_schedules_day_id_foreign` FOREIGN KEY (`day_id`) REFERENCES `days` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dcotor_schedules_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `doctor_profiles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dcotor_schedules_hospital_id_foreign` FOREIGN KEY (`hospital_id`) REFERENCES `hospital` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `diagnostic_center_tests`
--
ALTER TABLE `diagnostic_center_tests`
  ADD CONSTRAINT `diagnostic_center_tests_diagnostic_center_id_foreign` FOREIGN KEY (`Diagnostic_Center_id`) REFERENCES `diagnostic_center` (`id`),
  ADD CONSTRAINT `diagnostic_center_tests_tests_id_foreign` FOREIGN KEY (`Tests_id`) REFERENCES `tests` (`id`);

--
-- Constraints for table `doctor_hospitals`
--
ALTER TABLE `doctor_hospitals`
  ADD CONSTRAINT `doctor_hospitals_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `doctor_profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `doctor_hospitals_hospital_id_foreign` FOREIGN KEY (`hospital_id`) REFERENCES `hospital` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `seakers`
--
ALTER TABLE `seakers`
  ADD CONSTRAINT `seakers_donor_id_foreign` FOREIGN KEY (`donor_id`) REFERENCES `donars` (`id`);

--
-- Constraints for table `_testorder`
--
ALTER TABLE `_testorder`
  ADD CONSTRAINT `_testorder_tests_id_foreign` FOREIGN KEY (`Tests_id`) REFERENCES `tests` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
